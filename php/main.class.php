<?php
ob_start();

    require_once("session.class.php");
    class main {
        public function __construct() {
            header('Content-Type: text/html; charset=iso-8859-1');
            session_start();            
            if (class_exists('session')) {
                $this->session = new session($this);
            } else {
                die("Class session not exists!");
            } 
        }
    }
?>

