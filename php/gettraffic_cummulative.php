<?php

$d =  getTrafficResourceId($_GET['resourceid'],6);
$dd = getOutTrafficUtil($d);
echo "<br>";
$ddd = getInTrafficUtil($d);

function getTrafficResourceId($traffResourceId, $numberOfInstance){
	$data_string ='{"limit":'.$numberOfInstance.'}';
	$ch = curl_init('http://219.93.8.8:8777/v2/meters/network.outgoing.bytes'); 

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_GET['tokenid'],'Content-Type: application/json'));
	$result = curl_exec($ch);
	$error = curl_error($ch);
	curl_close ($ch);
	$obj = json_decode($result);
	$trafficResourceId = "-";
	if (sizeof($obj) > 0){
  		foreach ($obj as $name => $value) {
			if ($traffResourceId == $value->resource_metadata->instance_id)
				$trafficResourceId = $value->resource_id;
			//print_r($value);
  		}
	}
	return $trafficResourceId;
}

function getOutTrafficUtil($traffresourceid) {
	$data_string ='{"q": [{"field": "resource_id","op": "eq","type": "","value": "'.$traffresourceid.'"}],"limit":10}';
	$ch = curl_init('http://219.93.8.8:8777/v2/meters/network.outgoing.bytes'); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_GET['tokenid'],'Content-Type: application/json'));
	$result = curl_exec($ch);
	$error = curl_error($ch);
	curl_close ($ch);
	$obj = json_decode($result);
	$arrTrafficUtil = array();
	if (sizeof($obj) > 0){
  		foreach ($obj as $name => $value) {
			echo $value->timestamp." - ".$value->counter_volume."<br>";
			$arrTrafficUtil[] = array('dt'=>$value->timestamp,'oTraffic'=>$value->counter_volume);
  		}
	}
	$data = json_encode($arrTrafficUtil);

	return $data;
};

function getInTrafficUtil($traffresourceid) {
	$data_string ='{"q": [{"field": "resource_id","op": "eq","type": "","value": "'.$traffresourceid.'"}],"limit":10}';
	$ch = curl_init('http://219.93.8.8:8777/v2/meters/network.incoming.bytes'); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_GET['tokenid'],'Content-Type: application/json'));
	$result = curl_exec($ch);
	$error = curl_error($ch);
	curl_close ($ch);
	$obj = json_decode($result);
	$arrTrafficUtil = array();
	if (sizeof($obj) > 0){
  		foreach ($obj as $name => $value) {
			echo $value->timestamp." - ".$value->counter_volume."<br>";
			$arrTrafficUtil[] = array('dt'=>$value->timestamp,'oTraffic'=>$value->counter_volume);
  		}
	}
	$data = json_encode($arrTrafficUtil);

	return $data;
};

?>