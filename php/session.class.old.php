<?php
    class session {

        function islogin() {
		if (isSet($_SESSION['userid'])){
			return true;
		}else
			return false;
        }
        
        function getUserId() {
            return $_SESSION['userid'];
        }
		
		function getTokenId() {
            return $_SESSION['tokenid'];
        }
		
	 function getAccountId() {
            return $_SESSION['accountid'];
        }

	 function getControllerIp() {
            return $_SESSION['ctrlip'];
        }

        public function login($userid, $passw, $ctrlip) {
			
            	$response = "{success: false}";
            	$data_string ='{"auth":{"passwordCredentials":{"username":"'.$userid.'","password":"'.$passw.'"},"tenantName":"admin"}}';
		$ch = curl_init('http://'.$ctrlip.':5000/v2.0/tokens');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	       curl_setopt($ch,CURLOPT_TIMEOUT,1000);
		$result = curl_exec($ch);
		//$error = curl_error($ch);
		curl_close ($ch); 
		if ($result != ""){
			$_SESSION['userid'] = $userid;
			$_SESSION['ctrlip'] = $ctrlip;
			$obj = json_decode($result);
			if ($obj->access->token->id){
				$_SESSION['tokenid'] = $obj->access->token->id;
				$_SESSION['accountid'] = $obj->access->token->tenant->id;
				$response = '{success: true, msg:'.$result.'}';
			}else
				$response = '{success: false, msg:'.$result.'}';
		}else
			$response = '{success: false, msg:"Failed to logon. Please contact your system administrator."}';
            	return $response;
        }
	
        public function logout() {
            	session_destroy();
            	unset($_SESSION['userid']);
		unset($_SESSION['tokenid']);
		unset($_SESSION['accountid']);
		unset($_SESSION['ctrlip']);
        }
        
	 public function getOpenStackInstanceList() {
            //$result = '{"images":[{"name":"server-1","ipaddrs":"10.10.0.3"},{"name":"server-2","ipaddrs":"10.10.0.6"},{"name":"server-3","ipaddrs":"10.10.0.9"}]}';
		
		$tokid = $_SESSION['tokenid'];
		$accid = $_SESSION['accountid'];
		$ctrlip = $_SESSION['ctrlip'];
		$ch = curl_init('http://'.$ctrlip.':8774/v2/'.$accid.'/servers/detail');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokid));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch); 
		$arr = array();
		$isSessionExpired = 1;
		if ($result != "Authentication required"){
			$isSessionExpired = 0;
			$obj = json_decode($result);
			$eotm = $obj->servers;
			$countArr = 0;
  			foreach ($eotm as $name => $value) {
				if ($value->addresses->public[0])
					$tmpIpAddrs = $value->addresses->public[0]->addr;
				else
					$tmpIpAddrs = $value->addresses->private[0]->addr;
				$arr[$countArr] = array('id' => $value->id, 'name' => $value->name, 'ipaddrs' => $tmpIpAddrs, 'status' => $value->status);
				$countArr++;
     				//print_r($value);
  			}
		}
		return '{"is_session_expired":'.$isSessionExpired.',"images":'.json_encode($arr).'}';		
		//return $result;
        }

	 public function getTrafficUtil($numofInstance, $resourceid) {
		$resourceInterfaceId = $this->getTrafficResourceId($resourceid,$numofInstance);
		$minus8HoursDate = strtotime( date("Y-m-d H:i:s", strtotime(date('Y-m-d')." 00:00:00")) . " -7 hours");

		$arrOutTraff = "[".$this->getOutTrafficUtil($resourceInterfaceId, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
		$arrInTraff = "[".$this->getInTrafficUtil($resourceInterfaceId, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
		$traffData =  "[{
                                'id':'incoming',
                                 name: 'Incoming',
                		     color:'#5e8bc0',
				     lineWidth: 2,
				     marker: {
					fillColor: 'white',
					symbol:'circle',
					lineWidth: 2,
					lineColor: '#5e8bc0'
				     },
                             'data':";
		$traffData .= $arrOutTraff;
		$traffData .= "},{'id':'outgoing',
                                 name: 'Outgoing',
                		     color:'#DF5353',
				     lineWidth: 2,
				     marker: {
					fillColor: 'white',
					lineWidth: 2,
					symbol:'circle',
					lineColor: '#DF5353'
				     },
                               'data':";
		$traffData .= $arrInTraff;
		$traffData .= "}]";
		return $traffData;
	 }

	 function getTrafficResourceId($traffResourceId, $numberOfInstance){
		$data_string ='{"limit":'.$numberOfInstance.'}';
		$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.outgoing.bytes'); 

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch);
		$obj = json_decode($result);
		$trafficResourceId = "-";
		if (sizeof($obj) > 0){
  			foreach ($obj as $name => $value) {
				if ($traffResourceId == $value->resource_metadata->instance_id)
					$trafficResourceId = $value->resource_id;
				//print_r($value);
  			}
		}
		return $trafficResourceId;
	 }

	 function getOutTrafficUtil($traffresourceid,$tStamp) {
		$data_string ='{"q": [{"field": "timestamp","op": "gt","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$traffresourceid.'"}],"period":3600}';
		$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.outgoing.bytes/statistics'); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch);
		$obj = json_decode($result);
		$arrTrafficUtil = array();
		$data = "[]";
		$count= 0;
		if (sizeof($obj) > 0){
  			foreach ($obj as $name => $value) {
				if ($count == 0)
					$data = "[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
				else
					$data .= ",[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
				$count++;
  			}
		}
		return $data;
	 }

	 function getInTrafficUtil($traffresourceid, $tStamp) {
		$data_string ='{"q": [{"field": "timestamp","op": "gt","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$traffresourceid.'"}],"period":3600}';
		$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.incoming.bytes/statistics'); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch);
		$obj = json_decode($result);
		$arrTrafficUtil = array();
		$data = "[]";
		$count= 0;
		if (sizeof($obj) > 0){
  			foreach ($obj as $name => $value) {
				if ($count == 0)
					$data = "[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
				else
					$data .= ",[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
				$count++;
  			}
		}
		return $data;
	 }

	 function getPlus8HoursTimeStamp($timeStamp){
		$arrTimeStamp = explode('T',$timeStamp);
		$tStamp = $arrTimeStamp[0]." ".$arrTimeStamp[1];
		$tStampPlus8 = strtotime( date("Y-m-d H:i:s", strtotime($tStamp)) . " +8 hours");
		//Date.UTC(2015, 5-1, 22 ,01, 27)
		return date('\D\a\t\e\.\U\T\C\(Y\,m\-\1\,d\,H\,i\,s\)',$tStampPlus8);
	 }


	 public function getOpenStackDiskUtil($resourceid) {
		$data_string ='{"q": [{"field": "resource_id","op": "eq","type": "","value": "'.$resourceid.'"}],"limit":1}';
		$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/disk.usage'); 

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch);
		$obj = json_decode($result);
		$diskUtil = 0;
		$timeStamp = "-";
		if (sizeof($obj) > 0){
			$diskCapacityGB = intval($obj[sizeof($obj)-1]->resource_metadata->disk_gb);
			$diskCapacityByte = $diskCapacityGB * 1073741824; //1073741824 = 1 gigabyte
			$diskUsage = intval($obj[sizeof($obj)-1]->counter_volume);
			$diskUtil = ($diskUsage/$diskCapacityByte)*100;
		}
		return '{"diskutil":'.round($diskUtil , 2).',"totdisk":"'.$diskCapacityGB.' GB","timestamp":"'.date('d-M-y h:i:s A').'"}';		
		//return $result;
        }

	 public function getOpenStackMemUtil($resourceid) {
		date_default_timezone_set("Asia/Kuala_Lumpur");
		$ch = curl_init('http://'.$_SESSION['ctrlip'].':8774/v2/'.$_SESSION['accountid'].'/servers/'.$resourceid.'/diagnostics');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch); 
		$memUtil = 0;
		$memActualMb = "-";
		$isSessionExpired = 1;
		if ($result != "Authentication required"){
			$obj = json_decode($result);
			$memRss = intval($obj->{'memory-rss'});
			$memActual = intval($obj->{'memory-actual'});
			if (($memActual/1024) > 1024)
				$memActualMb = (($memActual/1024)/1024)." GB";
			else
				$memActualMb = ($memActual/1024)." MB";
			$memUtil = ($memRss/$memActual)*100;
			$isSessionExpired = 0;	
		}
		return '{"memutil":'.round($memUtil , 2).',"totmem":"'.$memActualMb.'","is_session_expired":'.$isSessionExpired.',"timestamp":"'.date('d-M-y h:i:s A').'"}';				
	 }

	 public function getOpenStackCpuUtil($resourceid) {
		$data_string ='{"q": [{"field": "resource_id","op": "eq","type": "","value": "'.$resourceid.'"}],"limit":1}';
		$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/cpu_util'); 

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch);
		$obj = json_decode($result);
		$cpuUtil = 0;
		$timeStamp = "-";
		$vCpus = "-";
		if (sizeof($obj) > 0){
			$cpuUtil = $obj[sizeof($obj)-1]->counter_volume;
			$vCpus = $obj[sizeof($obj)-1]->resource_metadata->{'flavor.vcpus'};
			$arrTimestamp = explode('T',$obj[sizeof($obj)-1]->timestamp);
			$timeStampPlus8 = strtotime(date("Y-m-d H:i:s", strtotime($arrTimestamp[0]." ".$arrTimestamp[1])) . " +8 hours");
		}
		return '{"cpuutil":'.round($cpuUtil, 2).',"vcpus":"'.$vCpus.'","timestamp":"'.date('d-M-y h:i:s A',$timeStampPlus8).'"}';		
		//return $result;
        }

	 public function getOpenStackCpuUtil_old($resourceid) {
		date_default_timezone_set("Asia/Kuala_Lumpur"); 
		$data_string ='{"q": [{"field": "timestamp","op": "ge","value": "'.date('Y-m-d').'T00:00:00"},{"field": "timestamp","op": "lt","value": "'.date('Y-m-d').'T23:59:59"},{"field": "resource_id","op": "eq","value": "'.$resourceid.'"}],"groupby": ["project_id", "resource_id"],"period":120}';
		$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/cpu_util/statistics'); 

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
		$result = curl_exec($ch);
		$error = curl_error($ch);
		curl_close ($ch); 
		$obj = json_decode($result);
		$cpuAvg = 0;
		$pStart = "-";
		$pEnd = "-";
		if (sizeof($obj) > 0){
			$cpuAvg = $obj[sizeof($obj)-1]->avg;
			$pStart = $obj[sizeof($obj)-1]->period_start;
			$pEnd = $obj[sizeof($obj)-1]->period_end;
		}
		if ($pStart != "-" && $pEnd != "-"){
			$arrPstart = explode('T',$pStart);
			$arrPend = explode('T',$pEnd);
			//$dd = new Date();
			$pStart = $arrPstart[0]." ".$arrPstart[1];
			$startDate = strtotime( date("Y-m-d H:i:s", strtotime($pStart)) . " +8 hours");
			$pEnd = $arrPend[0]." ".$arrPend[1];
			$endDate = strtotime( date("Y-m-d H:i:s", strtotime($pEnd )) . " +8 hours");
		}
		//return round($cpuAvg, 2);
		return '{"cpuutil":'.round($cpuAvg, 2).',"curr_dt":"'.date('Y-m-d H:i:s').'","period_start":"'.date('Y-m-d H:i:s',$startDate).'","period_end":"'.date('Y-m-d H:i:s',$endDate).'"}';		
        }

       
    }
?>
