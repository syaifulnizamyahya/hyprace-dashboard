<?php
    class session {

        function islogin() {
			if (isSet($_SESSION['userid'])){
					return true;
			}else
					return false;
        }
        
        function getUserId() {
            return $_SESSION['userid'];
        }
		
	 function getTenantDetails() {
            return $_SESSION['tenantdetails'];
        }
		
	 function getProjectMenu() {
            return $_SESSION['projectmenu'];
        }
		
	 function getNetworkDetails() {
            //return $_SESSION['networkdetails'];
		return $_SESSION['netdetails'];
        }
		
	 function getInstanceNetDetails() {
            return $_SESSION['instance_netdetails'];
        }

        public function login($userid, $passw, $ctrlip) {
			$_SESSION['userid'] = $userid;
			$_SESSION['ctrlip'] = $ctrlip;            
			$result = $this->getAuthenticateToken($userid, $passw, $ctrlip, "");
			$obj = json_decode($result);
			//$this->setNetworkDetails($obj->access->token->id);
			$objTenant = json_decode($this->getTenantList($obj->access->token->id));
			$arrTenantDetails = array();
			$arrProjectMenu = array();
			$arrNetDetails = array();
			$defaultProjDefaultId = "";
			$defaultProjDefaultName = "";
			$countArr = 0;
			foreach ($objTenant->tenants as $name => $value) {
				$result = $this->getAuthenticateToken($userid, $passw, $ctrlip, $value->id);
				$objTokenTenant = json_decode($result);
				//$this->setNetworkDetails($objTokenTenant->access->token->id);
				$arrNetDetails[$countArr] = array('tenantid' => $value->id, 'netdetails' => $this->getNetDetails($objTokenTenant->access->token->id));
				$arrTenantDetails[$countArr] = array('tokenid' => $objTokenTenant->access->token->id, 'tenantname' => $value->name, 'accountid' => $value->id);
				$arrProjectMenu[$countArr] = array('id' => $value->id, 
									'text' => $value->name, 'iconCls' => ($countArr>0?'':'displaycheck'));
				if ($countArr==0) {
					$defaultProjDefaultId = $value->id;
					$defaultProjDefaultName = $value->name;
				}
				$countArr++;
  			}
			$_SESSION['projectmenu'] = '{"projdefaultname":"'.$defaultProjDefaultName.'","projdefaultid":"'.$defaultProjDefaultId.
			  			      '","projectmenu":'.json_encode($arrProjectMenu).'}';
			$_SESSION['tenantdetails'] = $arrTenantDetails;
			$_SESSION['netdetails'] = $arrNetDetails;
			$response = '{success: true, msg:{"access":"success"}}';
            return $response;
        }
		/*
		function setNetworkDetails($tokId){		
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':9696/v2.0/networks'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			//echo $result;
			$obj = json_decode($result);
			$arrNetworkDetails = array();
			$countArr = 0;
			foreach ($obj->networks as $name => $value) {
				$arrNetworkDetails[$countArr] = array('id' => $value->id, 'name' => $value->name, 'status' => $value->status, 'subnet' => $value->subnets[0]);
				$countArr++;
  			}
			$_SESSION['networkdetails'] = $arrNetworkDetails;
		}*/
		
		function getNetDetails($tokId){		
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':9696/v2.0/networks'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			//echo $result;
			$obj = json_decode($result);
			$arrNetworkDetails = array();
			$countArr = 0;
			foreach ($obj->networks as $name => $value) {
				$arrNetworkDetails[$countArr] = array('id' => $value->id, 'name' => $value->name, 'status' => $value->status, 'subnet' => $value->subnets[0]);
				$countArr++;
  			}
			return $arrNetworkDetails;
		}

		function getSubNetDetails($tokId){		
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':9696/v2.0/subnets'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			echo $result;
			/*$obj = json_decode($result);
			$arrNetworkDetails = array();
			$countArr = 0;
			foreach ($obj->networks as $name => $value) {
				$arrNetworkDetails[$countArr] = array('id' => $value->id, 'name' => $value->name, 'status' => $value->status, 'subnet' => $value->subnets[0]);
				$countArr++;
  			}
			return $arrNetworkDetails;*/
		}

		
		function getAuthenticateToken($userName,$userPass,$ctrlip,$tenantId){
			$data_string ='{"auth":{"passwordCredentials":{"username":"'.$userName.'","password":"'.$userPass.'"},"tenantId":"'.$tenantId.'"}}';
			$ch = curl_init('http://'.$ctrlip.':5000/v2.0/tokens');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			   curl_setopt($ch,CURLOPT_TIMEOUT,1000);
			$result = curl_exec($ch);
			//$error = curl_error($ch);
			curl_close ($ch); 
			return $result;
		}
		
		function getTenantList($tokenId){
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':5000/v2.0/tenants'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			return $result;
		}
	
        public function logout() {
            	session_destroy();
            	unset($_SESSION['userid']);
		unset($_SESSION['ctrlip']);
		unset($_SESSION['tenantdetails']);
		unset($_SESSION['projectmenu']);
		//unset($_SESSION['networkdetails']);
		unset($_SESSION['instance_netdetails']);
		unset($_SESSION['projectmenu']);
		unset($_SESSION['netdetails']);
        }
        
		public function getOpenStackInstanceList($accountId) {
			$arr = array();
			$arrNetDetails = array();
			$isSessionExpired = 1; 
			foreach($_SESSION['tenantdetails'] as $key=>$value)	{
				if ($accountId == $value['accountid']){
					$result = $this->getInstanceList($value['tokenid'],$value['accountid']);
					if ($result != "Authentication required"){
						$isSessionExpired = 0;
						$obj = json_decode($result);
						$eotm = $obj->servers;
						$countArr = 0;
						foreach ($eotm as $name => $value) {
							$tmpNetAddrs = $this->getNetworkAddress($value->addresses,$accountId);
							$tmpAddrsList = "";
							$countAddrsList = 0;
							foreach($tmpNetAddrs as $key=>$value3){
								if ($countAddrsList == 0)
									$tmpAddrsList = $value3['ipaddrs']." (".$value3['networkname'].")";
								else
									$tmpAddrsList .= ", ".$value3['ipaddrs']." (".$value3['networkname'].")";
								$countAddrsList++;
								//print_r($value3);
							}
							$arrNetDetails[$countArr] = array('id' => $value->id, 'name' => $value->name, 'ntaddrs' => $tmpNetAddrs);
							$tmpIpAddrs = "-";
							$arr[$countArr] = array('id' => $value->id, 'name' => $value->name, 'ipaddrs' => $tmpAddrsList, 
												    'status' => $value->status, 'severity' => rand(1,3));
							$countArr++;
						}
					}
				}
			}
			//echo json_encode($arrNetDetails);
			$_SESSION['instance_netdetails'] = $arrNetDetails;
			return '{"is_session_expired":'.$isSessionExpired.',"images":'.json_encode($arr).'}';
        }
		
		function getNetworkAddress($netAddrs,$tenantId){
			$tmpNetDetails;
			$arrNetworkAddress = array();
			$countArr = 0;
			//#### get netdetails according to tenant id #####
			foreach($_SESSION['netdetails'] as $key=>$value){
				if ($tenantId == $value['tenantid']){
					$tmpNetDetails = $value['netdetails'];
					break;
				}
			}
			
			foreach($tmpNetDetails as $key=>$value){ //loop throught network list
				if (array_key_exists($value['name'], $netAddrs)) { 
					foreach($netAddrs->{$value['name']} as $key=>$value2){ //loop throught network address in current instance
							$arrNetworkAddress[$countArr] = array('networkname' => $value['name'], 'ipaddrs' => $value2->addr);
							$countArr++;
					}
				} 
			}
			/*
			foreach($_SESSION['networkdetails'] as $key=>$value){ //loop throught network list
				if (array_key_exists($value['name'], $netAddrs)) { 
					foreach($netAddrs->{$value['name']} as $key=>$value2){ //loop throught network address in current instance
							$arrNetworkAddress[$countArr] = array('networkname' => $value['name'], 'ipaddrs' => $value2->addr);
							$countArr++;
					}
				} 
			}
			*/
			
			return $arrNetworkAddress;
		}
		
		 public function getInstanceList($tokid,$acctid) {	
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8774/v2/'.$acctid.'/servers/detail');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokid));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch); 			
			return $result;	
		 }

		 public function getDrDwUtil($projectid,$resourceid) {
			$minus8HoursDate = strtotime( date("Y-m-d H:i:s", strtotime(date('Y-m-d')." 00:00:00")) . " -7 hours");
	
			$arrDiskRead = "[".$this->getDiskReadUtil($projectid,$resourceid, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
			$arrDiskWrite = "[".$this->getDiskWriteUtil($projectid,$resourceid, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
			$drdwData =  "[{
						id:'diskread',
						name: 'Disk Read',
						color:'#5e8bc0',
						lineWidth: 2,
						marker: {
							fillColor: 'white',
							symbol:'circle',
							lineWidth: 2,
							lineColor: '#5e8bc0'
						},
						'data':";
			$drdwData .= $arrDiskRead;
			$drdwData .= "},{'id':'diskwrite',
					 name: 'Disk Write',
					 color:'#DF5353',
					 lineWidth: 2,
					 marker: {
						fillColor: 'white',
						lineWidth: 2,
						symbol:'circle',
						lineColor: '#DF5353'
					 },
					   'data':";
			$drdwData .= $arrDiskWrite;
			$drdwData .= "}]";
			return $drdwData;
		 }

		 function getDiskReadUtil($projectid, $resourceid,$tStamp) {
			$tokenId = $this->getTokenId($projectid);
			$data_string ='{"q": [{"field": "timestamp","op": "ge","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$resourceid.'"}],"period":3600}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/disk.read.bytes/statistics'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$data = "[]";
			$count= 0;
			if (sizeof($obj) > 0){
				foreach ($obj as $name => $value) {
					if ($count == 0)
						$data = "[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					else
						$data .= ",[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					$count++;
				}
			}
			return $data;
		 }

		 function getDiskWriteUtil($projectid, $resourceid,$tStamp) {
			$tokenId = $this->getTokenId($projectid);
			$data_string ='{"q": [{"field": "timestamp","op": "ge","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$resourceid.'"}],"period":3600}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/disk.write.bytes/statistics'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$data = "[]";
			$count= 0;
			if (sizeof($obj) > 0){
				foreach ($obj as $name => $value) {
					if ($count == 0)
						$data = "[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					else
						$data .= ",[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					$count++;
				}
			}
			return $data;
		 }
		
		 public function getTrafficUtil($projectid,$numofInstance, $resourceid) {
			$resourceInterfaceId = $this->getTrafficResourceId($projectid,$resourceid,$numofInstance);
			$minus8HoursDate = strtotime( date("Y-m-d H:i:s", strtotime(date('Y-m-d')." 00:00:00")) . " -7 hours");
	
			$arrOutTraff = "[".$this->getOutTrafficUtil($projectid,$resourceInterfaceId, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
			$arrInTraff = "[".$this->getInTrafficUtil($projectid,$resourceInterfaceId, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
			$traffData =  "[{
						id:'incoming',
						name: 'Incoming',
						color:'#5e8bc0',
						lineWidth: 2,
						marker: {
							fillColor: 'white',
							symbol:'circle',
							lineWidth: 2,
							lineColor: '#5e8bc0'
						},
						'data':";
							$traffData .= $arrOutTraff;
						$traffData .= "},{'id':'outgoing',
							 name: 'Outgoing',
								 color:'#DF5353',
						 lineWidth: 2,
						 marker: {
						fillColor: 'white',
						lineWidth: 2,
						symbol:'circle',
						lineColor: '#DF5353'
						 },
								   'data':";
			$traffData .= $arrInTraff;
			$traffData .= "}]";
			return $traffData;
		 }
	
		/*
		 function getTrafficResourceId($projectid, $traffResourceId, $numberOfInstance){
			$tokenId = $this->getTokenId($projectid);
			$data_string ='{"limit":'.$numberOfInstance.'}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.outgoing.bytes'); 
	
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$trafficResourceId = "-";
			if (sizeof($obj) > 0){
				foreach ($obj as $name => $value) {
					if ($traffResourceId == $value->resource_metadata->instance_id)
						$trafficResourceId = $value->resource_id;
					//print_r($value);
				}
			}
			return $trafficResourceId;
		 }
		*/
		function getTrafficResourceId($projectid, $traffResourceId, $numberOfInstance){
			$tokenId = $this->getTokenId($projectid);
			$traffic10MinLastDate = $this->getTraffic10MinLastDate($tokenId);
			$data_string ='{"q": [{"field": "timestamp","op": "gt","value": "'.$traffic10MinLastDate.'"}]}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.outgoing.bytes'); 
	
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$trafficResourceId = "-";
			if (sizeof($obj) > 0){
				foreach ($obj as $name => $value) {
					if ($traffResourceId == $value->resource_metadata->instance_id)
						$trafficResourceId = $value->resource_id;					
					//print_r($value);
				}
			}
			return $trafficResourceId;
		 }

		function getTraffic10MinLastDate($tokenId){
			$data_string ='{"limit":1}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.outgoing.bytes'); 
	
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$trafficLast10MinDate = date('Y-m-d\TH:i:s');
			if (sizeof($obj) > 0){
				foreach ($obj as $name => $value) {
					$trafficLast10MinDate = $value->timestamp;
					//print_r($value);
				}
			}
			$arrTrafficLast10MinDate  = explode('T',$trafficLast10MinDate);
			$tmpTrafficLast10MinDate = $arrTrafficLast10MinDate[0]." ".$arrTrafficLast10MinDate[1];
			$dtLast10MinDate = strtotime( date("Y-m-d H:i:s", strtotime($tmpTrafficLast10MinDate)) . " -10 minutes");
			return date('Y-m-d\TH:i:s',$dtLast10MinDate);
		 }

	
		 function getOutTrafficUtil($projectid, $traffresourceid,$tStamp) {
			$tokenId = $this->getTokenId($projectid);
			$data_string ='{"q": [{"field": "timestamp","op": "gt","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$traffresourceid.'"}],"period":3600}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.outgoing.bytes/statistics'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$arrTrafficUtil = array();
			$data = "[]";
			$count= 0;
			if (sizeof($obj) > 0){
				foreach ($obj as $name => $value) {
					if ($count == 0)
						$data = "[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					else
						$data .= ",[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					$count++;
				}
			}
			return $data;
		 }
	
		 function getInTrafficUtil($projectid, $traffresourceid, $tStamp) {
			$tokenId = $this->getTokenId($projectid);
			$data_string ='{"q": [{"field": "timestamp","op": "gt","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$traffresourceid.'"}],"period":3600}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/network.incoming.bytes/statistics'); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$arrTrafficUtil = array();
			$data = "[]";
			$count= 0;
			if (sizeof($obj) > 0){
				foreach ($obj as $name => $value) {
					if ($count == 0)
						$data = "[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					else
						$data .= ",[".$this->getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
					$count++;
				}
			}
			return $data;
		 }
	
		 function getPlus8HoursTimeStamp($timeStamp){
			$arrTimeStamp = explode('T',$timeStamp);
			$tStamp = $arrTimeStamp[0]." ".$arrTimeStamp[1];
			$tStampPlus8 = strtotime( date("Y-m-d H:i:s", strtotime($tStamp)) . " +8 hours");
			//Date.UTC(2015, 5-1, 22 ,01, 27)
			return date('\D\a\t\e\.\U\T\C\(Y\,m\-\1\,d\,H\,i\,s\)',$tStampPlus8);
		 }
	
	
		 public function getOpenStackDiskUtil($projectid, $resourceid) {
			$tokenId = $this->getTokenId($projectid);
			$data_string ='{"q": [{"field": "resource_id","op": "eq","type": "","value": "'.$resourceid.'"}],"limit":1}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/disk.usage'); 
	
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$diskUtil = 0;
			$timeStamp = "-";
			if (sizeof($obj) > 0){
				$diskCapacityGB = intval($obj[sizeof($obj)-1]->resource_metadata->disk_gb);
				$diskCapacityByte = $diskCapacityGB * 1073741824; //1073741824 = 1 gigabyte
				$diskUsage = intval($obj[sizeof($obj)-1]->counter_volume);
				$diskUtil = ($diskUsage/$diskCapacityByte)*100;
			}
			return '{"diskutil":'.round($diskUtil , 2).',"totdisk":"'.$diskCapacityGB.' GB","timestamp":"'.date('d-M-y h:i:s A').'"}';		
			//return $result;
		 }
	
		 public function getOpenStackMemUtil($projectid, $resourceid) {
			$tokenId = $this->getTokenId($projectid);
			$accountId = $this->getAccountId($projectid);
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8774/v2/'.$accountId.'/servers/'.$resourceid.'/diagnostics');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch); 
			$memUtil = 0;
			$memActualMb = "-";
			$isSessionExpired = 1;
			if ($result != "Authentication required"){
				$obj = json_decode($result);
				$memRss = intval($obj->{'memory-rss'});
				$memActual = intval($obj->{'memory-actual'});
				if (($memActual/1024) > 1024)
					$memActualMb = (($memActual/1024)/1024)." GB";
				else
					$memActualMb = ($memActual/1024)." MB";
				$memUtil = ($memRss/$memActual)*100;
				$isSessionExpired = 0;	
			}
			return '{"memutil":'.round($memUtil , 2).',"totmem":"'.$memActualMb.'","is_session_expired":'.$isSessionExpired.',"timestamp":"'.date('d-M-y h:i:s A').'"}';				
		 }
	
		 public function getOpenStackCpuUtil($projectid, $resourceid) {
			$tokenId = $this->getTokenId($projectid);
			$data_string ='{"q": [{"field": "resource_id","op": "eq","type": "","value": "'.$resourceid.'"}],"limit":1}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/cpu_util'); 
	
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$tokenId,'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch);
			$obj = json_decode($result);
			$cpuUtil = 0;
			$timeStamp = "-";
			$vCpus = "-";
			if (sizeof($obj) > 0){
				$cpuUtil = $obj[sizeof($obj)-1]->counter_volume;
				$vCpus = $obj[sizeof($obj)-1]->resource_metadata->{'flavor.vcpus'};
				$arrTimestamp = explode('T',$obj[sizeof($obj)-1]->timestamp);
				$timeStampPlus8 = strtotime(date("Y-m-d H:i:s", strtotime($arrTimestamp[0]." ".$arrTimestamp[1])) . " +8 hours");
			}
			return '{"cpuutil":'.round($cpuUtil, 2).',"vcpus":"'.$vCpus.'","timestamp":"'.date('d-M-y h:i:s A',$timeStampPlus8).'"}';		
			//return $result;
		 }

		 public function getTokenId($projectid) {
			$tokId = "";
			foreach($_SESSION['tenantdetails'] as $key=>$value){
				if ($projectid == $value['accountid']){
					$tokId = $value['tokenid'];
				}
			}
			return $tokId;
		 }

		 public function getAccountId($projectid) {
			$acctId = "";
			foreach($_SESSION['tenantdetails'] as $key=>$value){
				if ($projectid == $value['accountid']){
					$acctId = $value['accountid'];
				}
			}
			return $acctId;
		 }
	 
		 public function getOpenStackCpuUtil_old($resourceid) {
			date_default_timezone_set("Asia/Kuala_Lumpur"); 
			$data_string ='{"q": [{"field": "timestamp","op": "ge","value": "'.date('Y-m-d').'T00:00:00"},{"field": "timestamp","op": "lt","value": "'.date('Y-m-d').'T23:59:59"},{"field": "resource_id","op": "eq","value": "'.$resourceid.'"}],"groupby": ["project_id", "resource_id"],"period":120}';
			$ch = curl_init('http://'.$_SESSION['ctrlip'].':8777/v2/meters/cpu_util/statistics'); 
	
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_SESSION['tokenid'],'Content-Type: application/json'));
			$result = curl_exec($ch);
			$error = curl_error($ch);
			curl_close ($ch); 
			$obj = json_decode($result);
			$cpuAvg = 0;
			$pStart = "-";
			$pEnd = "-";
			if (sizeof($obj) > 0){
				$cpuAvg = $obj[sizeof($obj)-1]->avg;
				$pStart = $obj[sizeof($obj)-1]->period_start;
				$pEnd = $obj[sizeof($obj)-1]->period_end;
			}
			if ($pStart != "-" && $pEnd != "-"){
				$arrPstart = explode('T',$pStart);
				$arrPend = explode('T',$pEnd);
				//$dd = new Date();
				$pStart = $arrPstart[0]." ".$arrPstart[1];
				$startDate = strtotime( date("Y-m-d H:i:s", strtotime($pStart)) . " +8 hours");
				$pEnd = $arrPend[0]." ".$arrPend[1];
				$endDate = strtotime( date("Y-m-d H:i:s", strtotime($pEnd )) . " +8 hours");
			}
			//return round($cpuAvg, 2);
			return '{"cpuutil":'.round($cpuAvg, 2).',"curr_dt":"'.date('Y-m-d H:i:s').'","period_start":"'.date('Y-m-d H:i:s',$startDate).'","period_end":"'.date('Y-m-d H:i:s',$endDate).'"}';		
		}
		 
		
       
    }
?>
