<?php

$resourceInterfaceId =  getTrafficResourceId($_GET['resourceid'],6);
$minus8HoursDate = strtotime( date("Y-m-d H:i:s", strtotime(date('Y-m-d')." 00:00:00")) . " -7 hours");

$arrOutTraff = "[".getOutTrafficUtil($resourceInterfaceId, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
$arrInTraff = "[".getInTrafficUtil($resourceInterfaceId, date('Y-m-d\TH:i:s',$minus8HoursDate))."]";
echo "[{
                                    'id':'incoming',
                                    name: 'Incoming',
                					color:'#5e8bc0',
									lineWidth: 2,
									marker: {
										fillColor: 'white',
										symbol:'circle',
										lineWidth: 2,
										lineColor: '#5e8bc0'
									},
                                    'data':";
echo $arrOutTraff;
echo "},{'id':'outgoing',
                                    name: 'Outgoing',
                					color:'#DF5353',
									lineWidth: 2,
									marker: {
										fillColor: 'white',
										lineWidth: 2,
										symbol:'circle',
										lineColor: '#DF5353'
									},
                                    'data':";
echo $arrInTraff;
echo "}]";


function getTrafficResourceId($traffResourceId, $numberOfInstance){
	$data_string ='{"limit":'.$numberOfInstance.'}';
	$ch = curl_init('http://219.93.8.8:8777/v2/meters/network.outgoing.bytes'); 

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_GET['tokenid'],'Content-Type: application/json'));
	$result = curl_exec($ch);
	$error = curl_error($ch);
	curl_close ($ch);
	$obj = json_decode($result);
	$trafficResourceId = "-";
	if (sizeof($obj) > 0){
  		foreach ($obj as $name => $value) {
			if ($traffResourceId == $value->resource_metadata->instance_id)
				$trafficResourceId = $value->resource_id;
			//print_r($value);
  		}
	}
	return $trafficResourceId;
}

function getOutTrafficUtil($traffresourceid,$tStamp) {
	$data_string ='{"q": [{"field": "timestamp","op": "gt","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$traffresourceid.'"}],"period":3600}';
	$ch = curl_init('http://219.93.8.8:8777/v2/meters/network.outgoing.bytes/statistics'); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_GET['tokenid'],'Content-Type: application/json'));
	$result = curl_exec($ch);
	$error = curl_error($ch);
	curl_close ($ch);
	$obj = json_decode($result);
	$arrTrafficUtil = array();
	$data = "[]";
	$count= 0;
	if (sizeof($obj) > 0){
  		foreach ($obj as $name => $value) {
			if ($count == 0)
				$data = "[".getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
			else
				$data .= ",[".getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
			$count++;
  		}
	}
	return $data;
	
};

function getInTrafficUtil($traffresourceid, $tStamp) {
	$data_string ='{"q": [{"field": "timestamp","op": "gt","value": "'.$tStamp.'"},{"field": "resource_id","op": "eq","value": "'.$traffresourceid.'"}],"period":3600}';
	$ch = curl_init('http://219.93.8.8:8777/v2/meters/network.incoming.bytes/statistics'); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: '.$_GET['tokenid'],'Content-Type: application/json'));
	$result = curl_exec($ch);
	$error = curl_error($ch);
	curl_close ($ch);
	$obj = json_decode($result);
	$arrTrafficUtil = array();
	$data = "[]";
	$count= 0;
	if (sizeof($obj) > 0){
  		foreach ($obj as $name => $value) {
			if ($count == 0)
				$data = "[".getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
			else
				$data .= ",[".getPlus8HoursTimeStamp($value->duration_end).",".$value->sum."]";
			$count++;
  		}
	}
	return $data;
};

function getPlus8HoursTimeStamp($timeStamp){
	$arrTimeStamp = explode('T',$timeStamp);
	$tStamp = $arrTimeStamp[0]." ".$arrTimeStamp[1];
	$tStampPlus8 = strtotime( date("Y-m-d H:i:s", strtotime($tStamp)) . " +8 hours");
	//Date.UTC(2015, 5-1, 22 ,01, 27)
	return date('\D\a\t\e\.\U\T\C\(Y\,m\-\1\,d\,H\,i\,s\)',$tStampPlus8);
}

?>