Ext.onReady(function(){				 
					 
	 var frmLogin = new Ext.FormPanel({
        labelWidth: 75, 
		baseCls: 'x-plain',
        bodyStyle:'padding:160px 84px 0',
        width: 394,
        defaults: {width: 150},
        defaultType: 'textfield',
		buttonAlign: 'center',
		renderTo : 'loginForm',
        items: [{
                fieldLabel: 'User ID',
		  id: 'username',
                name: 'username',
                allowBlank:false
            },{
                fieldLabel: 'Password',
		  id: 'password',
                name: 'password',
		  inputType: 'password',
		  allowBlank: false,
		  enableKeyEvents: true,
		  listeners: {
			keypress: function(field, e) {
				if (e.getKey() === Ext.EventObject.ENTER)
					beginAutentication();
			}
		  }		
            },{
                fieldLabel: 'Controller IP',
		  id: 'ctrlip',
                name: 'ctrlip',
		  enableKeyEvents: true,
		  allowBlank:false,
		  listeners: {
			keypress: function(field, e) {
				if (e.getKey() === Ext.EventObject.ENTER)
					beginAutentication();
			}
		  }
            }
        ],
        buttons: [{
            text: 'Login',
			handler: function(){
				beginAutentication();
			}
        },{
            text: 'Clear',
			handler: function(){
			   frmLogin.getForm().reset();
			}
        }]
    });
	
	function beginAutentication(){
		//var passMD5 = Ext.util.MD5(Ext.getCmp('password').getValue());
		//Ext.getCmp('password').setRawValue(passMD5);
		frmLogin.getForm().submit({
			url: 'php/login.php',
			method: 'POST',
			waitMsg: 'Authenticating...',
			success: function(form, action){
				if (action && action.result) {
					if (action.result.msg.access){
						window.location = './main.php'; 
					}else if (action.result.msg.error){
						Ext.MessageBox.show({
                					title: action.result.msg.error.title,
                					msg: action.result.msg.error.message,
                					buttons: Ext.MessageBox.OK,
                					icon: Ext.MessageBox.WARNING
             					});
					}
				}
			},
			failure:function(form, action){
				if (action.result.msg.error)
					Ext.MessageBox.show({
                				title: action.result.msg.error.title,
                				msg: action.result.msg.error.message,
                				buttons: Ext.MessageBox.OK,
                				icon: Ext.MessageBox.WARNING
             				});
				 else
				  	switch (action.failureType) {
						case Ext.form.Action.CLIENT_INVALID:
							Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
							break;
						case Ext.form.Action.CONNECT_FAILURE:
							Ext.Msg.alert('Failure', 'Ajax communication failed');
							break;
						case Ext.form.Action.SERVER_INVALID:
					   		Ext.Msg.alert('Failure', 'Server Invalid');
							break;
						default:
							Ext.Msg.alert('Failure', action.result.msg);
			   	 	}	
			}
		});
	}
	
	
});