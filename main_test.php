<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require_once("php/main.class.php");
$main = new main();
if(!$main->session->islogin()){
	print "<script>window.location.href = 'index.html';</script>";
	die;	
}
$class_methods = get_class_methods('session');


foreach ($class_methods as $method_name) {
    echo "$method_name\n";
}
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Hyprace</title>
    <link rel="stylesheet" type="text/css" href="../ext-3.2.1/resources/css/ext-all.css" />
	<link rel="stylesheet" type="text/css" href="../ext-3.2.1/resources/css/xtheme-gray.css" />
	<link rel="stylesheet" type="text/css" href="css/app-look-n-feel.css" />
	
    <script type="text/javascript" src="../ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="../ext-3.2.1/ext-all.js"></script>
	<script type="text/javascript" src="js/plugins/miframe.js"></script>
	<script type="text/javascript" src="js/main/main.js"></script>
	<script >var glbUserId = '<?php echo $main->session->getProjectMenu(); ?>'</script>
	<script >var glbProjectMenu = Ext.util.JSON.decode('{"projdefaultname":"admin","projdefaultid":"8a8cb405342f4e588065b074627cfea3","projectmenu":[{"id":"8a8cb405342f4e588065b074627cfea3","text":"&nbsp;admin&nbsp;","iconCls":"displaycheck"},{"id":"be5667d6246d4a899cb7e266d08d1a9d","text":"&nbsp;tenant1&nbsp;","iconCls":""}]}')</script>
</head>
<body>
<div id="header">
<div id="bannerbg"><div id="bannertitledeco"></div></div>
<div id="top_right_menu"> 
</div>
</body>
</html>
