<?php
require_once("php/main.class.php");
$main = new main();
if(!$main->session->islogin()){
	print "<script>window.location.href = 'index.html';</script>";
	die;	
}
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Hyprace</title>
    <link rel="stylesheet" type="text/css" href="../ext-3.2.1/resources/css/ext-all.css" />
	<link rel="stylesheet" type="text/css" href="../ext-3.2.1/resources/css/xtheme-gray.css" />
	<link rel="stylesheet" type="text/css" href="css/app-look-n-feel.css" />
	
    <script type="text/javascript" src="../ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="../ext-3.2.1/ext-all.js"></script>
	<script type="text/javascript" src="js/plugins/miframe.js"></script>
	<script type="text/javascript" src="js/main/main.js"></script>
	<script >var glbUserId = '<?php echo $main->session->getUserId(); ?>';
		  var glbProjectMenu = Ext.util.JSON.decode('<?php echo $main->session->getProjectMenu(); ?>');
	</script>
</head>
<body>
<div id="header">
<div id="bannerbg"><div id="bannertitledeco"></div></div>
<div id="top_right_menu"> 
</div>
</body>
</html>
