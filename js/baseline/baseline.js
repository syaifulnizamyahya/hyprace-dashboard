

Ext.onReady(function() {
    Ext.QuickTips.init();
    
    var getParams = document.URL.split("?");
    var paramVal = Ext.urlDecode(getParams[getParams.length - 1]);
    var editableVal = true;
 
    var baselineRowDataGrid = [
        {name: 'metric',type: 'string'},        
        {name: 'normal',type: 'string'},
        {name: 'major',type: 'string'},
		{name: 'critical',type: 'string'}        
    ];
	
    var baselineRecordObj = Ext.data.Record.create(baselineRowDataGrid);

    var baselineReader = new Ext.data.JsonReader( 
    {
        root: 'results',
        totalProperty: 'total'
    },
        baselineRecordObj//pass a reference to the object 
    );
        
    var baseline_Caption_groupRow = [
          {header: '', colspan: 1, align: 'center'},
          {header: 'Instance Performance Baseline', colspan: 3, align: 'center'}
    ];
    
    var baseline_groupRow = [
          {header: '', colspan: 1, align: 'center'},
          {header: 'Normal', colspan: 1, align: 'center'},
          {header: 'Major', colspan: 1, align: 'center'},
          {header: 'Critical', colspan: 1, align: 'center'}
    ];
    
    dsBaseline = new Ext.data.Store({     
        proxy: new Ext.data.HttpProxy({
            url: '../../php/getBaseline.php',  
            method: 'POST'
        }),
        autoLoad: true,
        reader: baselineReader
    });	
    
    var group = new Ext.ux.grid.ColumnHeaderGroup({
        rows: [baseline_Caption_groupRow]
    });
    
    function formatMetric(value, metaData, record){
        metaData.css = "metriccaption";
		return '<font color="#464646" face="Tahoma">' + value +'</font>';
    };
    
    function formatNormalSeverity(value, metaData, record){
        metaData.css = "rating_normal";
        return value ? '<font face="Tahoma">< ' +value + '%</font>' : '' ;
    };
    
    function formatMajorSeverity(value, metaData, record){
        metaData.css = "rating_major";
        return value ? 'Between ' + record.data.normal + '% to ' + record.data.critical + '%' : '' ;
    };
    
    function formatCriticalSeverity(value, metaData, record){
        metaData.css = "rating_critical";
        return value ? '<font face="Tahoma">> ' + value + '%</font>' : '' ;
    };
    

    function update_baseline(baseline_json) {
        Ext.Ajax.request({   
            url: '../../php/setBaseline.php',
            params: { 
                baseline: baseline_json
            },
            callback: function (options, success, response) {
                var json = Ext.util.JSON.decode(response.responseText);
                if (json.success) {                         
                        dsBaseline.commitChanges();
						dsBaseline.reload();
						Ext.MessageBox.show({
							title: 'Success',
							msg: 'Baseline updated successfully.',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.INFO
						});
                };	
            }
        }); //end request
		
    };
         
    var BaselineDataGrid = new Ext.grid.EditorGridPanel({
        border:false,
        region:'center',
        stripeRows: true,	
        columnLines:true,
        iconCls:'baseline',
        store: dsBaseline,
        loadMask: {msg:'Loading ...'},
        viewConfig: {
            emptyText: 'No baseline to display'     
        },
        columns: [
            {header: 'Metric', width: 100, fixed:true, resizable: true, align: 'center', 
             menuDisabled: true,sortable: false, dataIndex: 'metric', renderer:formatMetric},
            {header: 'Normal', width: 100, align: 'center', menuDisabled: true,
             sortable: true, dataIndex: 'normal', renderer:formatNormalSeverity,editable: editableVal,
             editor: new Ext.ux.form.SpinnerField({
                minValue: 1,
            	maxValue: 870,
            	allowDecimals: false,
            	accelerate: true
             })
            }, 
			{header: 'Major', width: 200, align: 'center', menuDisabled: true,
             sortable: true, dataIndex: 'major', renderer:formatMajorSeverity,editable: false,
             editor: new Ext.ux.form.SpinnerField({
                minValue: 1,
            	maxValue: 870,
            	allowDecimals: false,
            	accelerate: true
             })
            },
            {header: 'Critical', width: 100, align: 'center', menuDisabled: true,
             sortable: true, dataIndex: 'critical', renderer:formatCriticalSeverity,editable: editableVal,
             editor: new Ext.ux.form.SpinnerField({
                minValue: 1,
            	maxValue: 870,
            	allowDecimals: false,
            	accelerate: true
             })
            }
        ],
        plugins: group,
        listeners: {	
            afteredit: function(e) {
				var data = [];
				dsBaseline.each(function(rec){
				   data.push(rec.data);
				});
				update_baseline(Ext.util.JSON.encode(data));
			/*
			refStore.removeAll();
					if (response.responseText != ''){
						var json = Ext.util.JSON.decode(response.responseText);
						for(i=0;i<json.length;i++){
							refStore.add(new refRecordObj({			   
								refid: json[i].refid,
								reftitle: json[i].reftitle,
								author: json[i].author,
								reftype:json[i].reftype
							})); 
						}
					}
			*/

            }
        }
    });
    
   
    var viewport = new Ext.Viewport({
        layout:'border',
        renderTo: Ext.getBody(),
        items:[
           BaselineDataGrid
         ]
    });	
});