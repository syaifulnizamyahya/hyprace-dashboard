Ext.onReady(function(){
	var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});
	var getParams = document.URL.split("/");
	var serverURL = getParams[0]+"//"+getParams[2];
	
	var store = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../php/getinstancelist.php', 
            method: 'POST',
            headers: {},
            timeout: 60000
        }),
		listeners: {
			load: {
            		fn: function(store, records, options ){
						if (store.reader.jsonData.is_session_expired)
							parent.postMessage(
								'session_expired',
								serverURL
							);
						else
							dvInstanceList.select(0);
            		}
            	}
		},
        autoLoad: true,
        reader: new Ext.data.JsonReader( 
        {
            root: 'images'
        },[   
	     {name: 'id'},	
            {name: 'name'},
            {name: 'ipaddrs'},
    		 {name: 'status'}
        ])
    });
	
	var tpl = new Ext.XTemplate(
		'<tpl for=".">',
            '<div class="thumb-wrap" id="{id}">',
		    '<div class="thumb"><img src="server.png" title="{name}"></div>',
		    '<span class="x-editable"><b>{shortName}</b></span>',
		    '<span>{ipaddrs}</span>',
		    '<span>{status}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
	);
   
    var dvInstanceList = new Ext.DataView({
		id:'dvInstanceList',				
		store: store,
		tpl: tpl,
		autoScroll  : true,
		singleSelect: true,
		overClass:'x-view-over',
		itemSelector:'div.thumb-wrap',
		emptyText: 'No instances to display',
		prepareData: function(data){
			data.shortName = Ext.util.Format.ellipsis(data.name, 15);
			//data.sizeString = Ext.util.Format.fileSize(data.size);
//                data.dateString = data.lastmod.format("m/d/Y g:i a");
			return data;
		},
		
		listeners: {
			selectionchange: {
				fn: function(dv,nodes){
					if (nodes[0].id){
						var selInstance = store.getById(nodes[0].id);
						viewport.get('perf_metrics').setTitle('<center>'+
							'Performance Metrics for '+selInstance.json.name+' ('+selInstance.json.ipaddrs+')</center>');
						loadMask.show();
						Ext.Ajax.request({   
							url: '../../php/getcpuutil.php',
							params: {
								resourceid: nodes[0].id
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								var cUtil = CpuUtilanel.getComponent('Cpu_Util_Chart_Panel').getComponent('cpu_utilchart');
								CpuUtilanel.setTitle('<center>CPU Utilization ('+json.cpuutil+'%)</center>');
								//CpuUtilanel.getTopToolbar().findById('cTimestamp').setText('&nbsp;Timestamp: '+json.timestamp);
								cUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
								cUtil.chart.setTitle(null, { text: 'CPU : '+json.vcpus+' Core'});
								//cUtil.chart.setTitle(null, { text: 'CPU : Core'});
								cUtil.chart.series[0].points[0].update(parseFloat(json.cpuutil));
							}
						});
						Ext.Ajax.request({   
							url: '../../php/getmemutil.php',
							params: {
								resourceid: nodes[0].id
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								if (json.is_session_expired)
									parent.postMessage(
										'session_expired',
										serverURL
									);
								else{
									var mUtil = MemoryUtilPanel.getComponent('Mem_Util_Chart_Panel').getComponent('mem_utilchart');
									MemoryUtilPanel.setTitle('<center>Memory Utilization ('+json.memutil+'%)</center>');
									//MemoryUtilPanel.getTopToolbar().findById('mTimestamp').setText('&nbsp;Timestamp: '+json.timestamp);									
									mUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
									mUtil.chart.setTitle(null, { text: 'Total Memory : '+json.totmem});
									mUtil.chart.series[0].points[0].update(parseFloat(json.memutil));
								}
							}
						});
						Ext.Ajax.request({   
							url: '../../php/getdiskutil.php',
							params: {
								resourceid: nodes[0].id
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								var sUtil = StorageUtilPanel.getComponent('Storage_Util_Chart_Panel').getComponent('storage_utilchart');
								StorageUtilPanel.setTitle('<center>Disk Utilization ('+json.diskutil+'%)</center>');
								//StorageUtilPanel.getTopToolbar().findById('dTimestamp').setText('&nbsp;Timestamp: '+json.timestamp);
								sUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
								sUtil.chart.setTitle(null, { text: 'Total Disk : '+json.totdisk});
								sUtil.chart.series[0].points[0].update(parseFloat(json.diskutil));
								
							}
						});
					}	
				}
			}
		}
	});

    
	var CpuUtilanel = new Ext.Panel({
		id:'CpuUtilanel',
		layout: 'anchor',
		title:'<center>CPU Utilization (45%)</center>',
        	cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){}
                     }]        
		})
	});
	
	function drawCpuUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'cpu_utilchart',
			series: [{
				name: 'CPU Utilization',
				data: [80],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					//renderTo: 'container',
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		cpu_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Cpu_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 82%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('cpu_utilchart').setWidth(aw);
										Ext.getCmp('cpu_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				cpu_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('cpuUtilChartPanel', cpu_utilchartPanel); 
		CpuUtilanel.add({xtype: 'cpuUtilChartPanel'});
		CpuUtilanel.doLayout();
	}; //end of drawCpuUtilChart

	drawCpuUtilChart();
	
	var MemoryUtilPanel = new Ext.Panel({
		id:'MemoryUtilPanel',
		layout: 'anchor',
		title:'<center>Memory Utilization (34%)</center>',
        	cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){}
                     }]        
		})
	});
	
	function drawMemoryUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'mem_utilchart',
			series: [{
				name: 'Memory Utilization',
				data: [40],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		mem_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Mem_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 82%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('mem_utilchart').setWidth(aw);
										Ext.getCmp('mem_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				mem_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('memUtilChartPanel', mem_utilchartPanel); 
		MemoryUtilPanel.add({xtype: 'memUtilChartPanel'});
		MemoryUtilPanel.doLayout();
	}; //end of drawMemoryUtilChart

	drawMemoryUtilChart();
	
	var StorageUtilPanel = new Ext.Panel({
		id:'StorageUtilPanel',
		layout: 'anchor',
		title:'<center>Disk Utilization (58%)</center>',
        cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){}
                     }]        
		})
	});
	
	function drawStorageUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'storage_utilchart',
			series: [{
				name: 'Storage Utilization',
				data: [20],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		storage_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Storage_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 82%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('storage_utilchart').setWidth(aw);
										Ext.getCmp('storage_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				storage_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('storageUtilChartPanel', storage_utilchartPanel); 
		StorageUtilPanel.add({xtype: 'storageUtilChartPanel'});
		StorageUtilPanel.doLayout();
	}; //end of drawStorageUtilChart

	drawStorageUtilChart();
	
	
	var TrafficUtilPanel = new Ext.Panel({
		id:'TrafficUtilPanel',
		layout: 'anchor',
		title:'<center>Incoming And Outgoing Traffic Utilization</center>',
        cls:'home-panel',
		flex:1,
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
							cls: 'custombtn',
                            iconCls: 'speedohistory',height:34,
                            handler: function(){}
                     	},'->',new Ext.BoxComponent({
							autoEl: {
								cls: 'incoming'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),
						{xtype:'tbtext', 
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Incoming Traffic'
						},new Ext.Spacer({width:10}),new Ext.BoxComponent({
							autoEl: {
								cls: 'outgoing'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),{xtype:'tbtext', 
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Outgoing Traffic'
						},new Ext.Spacer({width:3})
					]        
		})
	});
	
	function drawTrafficUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'traffic_utilchart',
			series: [{
                name: 'Incoming',
                color:'#5e8bc0',
                data: [5, 3, 4, 7, 2]
            }, {
                name: 'Outgoing',
                color:'#DF5353',
                data: [2, 2, 3, 2, 1]
            }],
			chartConfig: {
				chart: {
					type: 'column'
				},
				title: {
					text: 'Data From 18-Aug-15 12:23:34 AM To 18-Aug-15 12:23:34 AM',
					style: {
						font: 'normal 12px verdana'
					}
				},
				xAxis: {
					title: {
						enabled: true
						,text: 'Date Time'
					},
					labels: {
						rotation: -45,
						align: 'right',
						style: {
								font: 'normal 8px Verdana, sans-serif'
						}
					},
					categories: ['3-Aug-15 12:22', '3-Aug-15 12:22', '3-Aug-15 12:22', '3-Aug-15 12:22', '3-Aug-15 12:22']
				},
				yAxis: {
					min: 0,
					title: {
						text: 'MB'
					},
					stackLabels: {
						enabled: true,
						style: {
							fontWeight: 'bold',
							color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
						}
					}
				},
				credits: {enabled:false},
				 tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },
			legend: {
				enabled: false
            },
            exporting: {
				buttons:{
					exportButton:{
						enabled: false
					 },
					 printButton: {
						 enabled: false
					 }
				  }
			},
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            }
			}
        });
		traffic_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Traffic_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 100%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('traffic_utilchart').setWidth(aw);
										Ext.getCmp('traffic_utilchart').setHeight(268);
									}
								},
								items: [chart]//end items
							}, config);
	
				traffic_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('trafficUtilChartPanel', traffic_utilchartPanel); 
		TrafficUtilPanel.add({xtype: 'trafficUtilChartPanel'});
		TrafficUtilPanel.doLayout();
	}; //end of drawTrafficUtilChart

	drawTrafficUtilChart();
	
	var InstanceSpeedoPanel = new Ext.Panel({
		id:'InstanceUtilPanel',
		style: 'background-color: #f1f1f1',
		border:false,
		layout:'column',
		//flex:1,
		width:750,
		height:500,
        autoScroll:true,
		items:[
			{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0px 5px 5px',
				items:CpuUtilanel
			},{
				columnWidth:.33, 
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0 5px 5px',
				items:MemoryUtilPanel
			},{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0 5px 5px',
				items:StorageUtilPanel
			}]
	});
		  
	var InstanceListPanel = new Ext.Panel({
        id:'images-view',
        anchor:'50% 30%',
        layout:'fit',
        title:'<center>Instances Summary</center>',
		cls:'home-panel',
        items: dvInstanceList
    });
	
	  
	var viewport = new Ext.Viewport({
			layout:'border',

			items: [{
				id:'instance_list',
				region:'north',
				baseCls:'x-plain',
				split:true,
				height:155,
				minHeight: 155,
				maxHeight: 200,
				layout:'fit',
				margins: '5 5 0 5',
				items: InstanceListPanel
		   },{
				id:'perf_metrics',
				region:'center',
				margins: '0 5 5 5',
				layout:'anchor',
				title:'<center>Performance Metrics</center>',
				cls:'home-panel',
				items:[{
					anchor:'100%',
					baseCls:'x-plain',
					layout:'hbox',
					layoutConfig: {
						padding:'5',
						align:'top'
					},
					defaults:{margins:'0 5 0 0'},
					items:[InstanceSpeedoPanel,
					TrafficUtilPanel
					]
                }]
          }]
	});  
	
});