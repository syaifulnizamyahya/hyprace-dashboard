Ext.onReady(function(){
	var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});
	var getParams = document.URL.split("/");
	var serverURL = getParams[0]+"//"+getParams[2];
	var splitDataParams = document.URL.split("?");
	var dataParam = Ext.urlDecode(splitDataParams[splitDataParams.length - 1]);
	var glbResourceId = "";
	var glbFlavorId = "";
	var glbSelInstanceName = "";
	var countSeverity1 = 0;
	var countSeverity2 = 0;
	var countSeverity3 = 0;
	
	loadMask.show();

	var flavorRecordObj = Ext.data.Record.create([   
		{name: 'id'},												 
		{name: 'name'},
		{name: 'cpu'},
		{name: 'ram'},
		{name: 'disk'}
	]);

	var flavor_store = new Ext.data.Store({
		proxy: new Ext.data.HttpProxy({
			url: '../../php/getflavorlist.php', 
			method: 'POST',
			headers: {},
			timeout: 60000
		}),
		baseParams: {
			projid: dataParam['projid']
		},
		autoLoad: true,
		reader: new Ext.data.JsonReader( 
		{
			root: 'results', 
			totalProperty: 'total'
		},flavorRecordObj)
	});
	
	var flavorCombo = new Ext.form.ComboBox({
			id:'cmbFlavor',
			fieldLabel: 'Name',
			store: flavor_store,
			displayField: 'name',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			emptyText:'Select a new flavor...',
			selectOnFocus:true,
			anchor: '100%',
			editable: false,
			//width:150,
			listeners:{
				select: function(combo, record, index) {
					//console.info(record.data.id);
					//console.dir(Ext.getCmp('nfcpu'));
					//Ext.get('cmbState').getValue()
					glbFlavorId = record.data.id;
				    	Ext.getCmp('nfcpu').setValue(record.data.cpu);
					Ext.getCmp('nfram').setValue(record.data.ram);
					Ext.getCmp('nfdisk').setValue(record.data.disk);
				}
			}
	});
	
	var scaleoutdata = [{
        bodyStyle: 'padding: 0px 5px 0px 5px;',
        items: {
            xtype: 'fieldset',
            title: 'Old Flavor : ',
            autoHeight: true,
            defaultType: 'textfield', 
            items: [{
				id: 'ofname',						
                name: 'ofname',
				readOnly : true,
                fieldLabel: 'Name',
				anchor: '100%'
            }, {
				id: 'ofcpu',
                name: 'ofcpu',
				readOnly : true,
                fieldLabel: 'CPU',
				anchor: '100%'
            }, {
				id: 'ofram',
                name: 'ofram',
				readOnly : true,
                fieldLabel: 'RAM',
				anchor: '100%'
            },{
				id: 'ofdisk',
				name: 'ofdisk',
				readOnly : true,
				fieldLabel: 'Disk',
				anchor: '100%'  
			}]
        }
    }, {
        bodyStyle: 'padding: 0px 5px 0px 5px;',
        items: {
            xtype: 'fieldset',
            title: 'New Flavor : ',
            autoHeight: true,
            defaultType: 'textfield', 
            items: [flavorCombo, {
				id: 'nfcpu',
                name: 'nfcpu',
				readOnly : true,
                fieldLabel: 'CPU',
				anchor: '100%'
            }, {
				id: 'nfram',
                name: 'nfram',
				readOnly : true,
                fieldLabel: 'RAM',
				anchor: '100%'
            },{
				id: 'nfdisk',
				name: 'nfdisk',
				readOnly : true,
				fieldLabel: 'Disk',
				anchor: '100%'  
			}]
        }
    }];
	
	var ScaleOutForm = new Ext.FormPanel({
        	labelWidth: 40,
		bodyStyle:'padding:1px 1px 0;',
		//style: 'background-color: #dfe8f6',
		defaults: {anchor: '100%'},
		baseCls: 'x-plain',
        	items: [
            	{
                layout: 'column',
                border: false,
		  baseCls: 'x-plain',
                defaults: {
                columnWidth: '.5',
		  baseCls: 'x-plain',
                border: false
                },            
                items: scaleoutdata
            	}]
   	});
	
	var winScaleOut = new Ext.Window({
		title: "&nbsp;Please select a new flavor to resize the instance",
		width: 590, 
		height:226, 
		iconCls:'resize',
		buttonAlign:'center',
		//closable :false,
		closeAction:'hide',
		draggable : false,
		resizable : false,
		x:(Ext.getBody().getWidth()/2)-300,
		y:10,
		border:true,
		plain: true,
		shadow : Ext.isIE?false:true,
		frame:true,
		bodyStyle:'padding:5px;',
		modal:true,
		layout: 'fit',
		items: [ScaleOutForm],
		buttons: [{
            		text: '&nbsp;Resize&nbsp;',
			iconCls:'resize',
			handler: function(){
				if (flavorCombo.getValue() != '')
            			        Ext.MessageBox.confirm('Confirmation','Are you sure you want to resize <b>'+
					  				           glbSelInstanceName+'</b> flavor<br>'+
									           'from <b>'+Ext.getCmp('ofname').getValue()+'</b> flavor '+
									           'to <b>'+flavorCombo.getValue()+'</b> flavor?', resizeConfirmDialog);
				else
					Ext.MessageBox.show({
                				title: 'New flavor is not selected',
				              msg: 'Please select the new flavor.',
                				buttons: Ext.MessageBox.OK,
                				icon: Ext.MessageBox.INFO
             				});
            		}
        	},{
            		text: '&nbsp;Close&nbsp;',
			iconCls:'close',
            		handler: function(){
				winScaleOut.hide();
            		}
	    	}]
	});

	function resizeConfirmDialog(btn){
        	if (btn == 'yes') {
            		confirmResize();    
        	}
    	};

       function confirmResize(){
	     loadMask.show();
	     winScaleOut.hide();
	     
            Ext.Ajax.request({
                url: '../../php/setResize.php',
		  params: {
			projid: dataParam['projid'],
			resourceid: glbResourceId,
			flavorid: glbFlavorId
		  },
                callback: function (options, success, response) {
                     loadMask.hide();
			var json = Ext.util.JSON.decode(response.responseText);			
			if (json.resizeStat.status == 'ok')				
				Ext.MessageBox.show({
                			title: 'Success',
				       msg: 'Instance successfully resized.',
                			buttons: Ext.MessageBox.OK,
                			icon: Ext.MessageBox.INFO,
					fn: function(btn){ 
						loadMask.show();
                            		dvInstanceList.store.reload();
						dvInstanceList.refresh();  
                         		}
             			});				
			else
				Ext.MessageBox.show({
                			title: 'Instance resizing failed',
				       msg: 'Unable to resize instance. Please contact system administrator.',
                			buttons: Ext.MessageBox.OK,
                			icon: Ext.MessageBox.WARNING
             			});					
                }
            }); //end request   
	     
    	};


	var store = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../php/getinstancelist.php', 
            method: 'POST',
            headers: {},
            timeout: 60000
        }),
		baseParams: {
			projid: dataParam['projid']
		},
		listeners: {			 
			load: {
            			fn: function(store, records, options ){
						loadMask.hide();						
						for (i=0;i<records.length;i++){
							if (records[i].data.severity == '1') countSeverity1++; //ijo
							if (records[i].data.severity == '2') countSeverity2++; //kunin
							if (records[i].data.severity == '3') countSeverity3++; //meroh
						}
						if (store.reader.jsonData.is_session_expired)
							parent.postMessage(
								'session_expired',
								serverURL
							);
						else{
							InstanceListPanel.getTopToolbar().findById('lblLastRefreshed').setText('Last Refreshed : '+
																store.reader.jsonData.timestamp);
							dvInstanceList.select(0);
						}
            		}
            	}
		},
        autoLoad: true,
        reader: new Ext.data.JsonReader( 
        {
            root: 'images'
        },[   
	     	{name: 'id'},	
              {name: 'name'},
              {name: 'ipaddrs'},
    		{name: 'status'},
		{name: 'severity'},
		{name: 'memutil'},
		{name: 'memtot'},
		{name: 'memdt'},
    		{name: 'cpuutil'},
		{name: 'cputot'},
		{name: 'cpudt'},
		{name: 'diskutil'},
		{name: 'disktot'},
		{name: 'diskdt'},
		{name: 'flavorid'},
		{name: 'flavorname'},
		{name: 'flavorcpu'},
		{name: 'flavormem'},
		{name: 'flavordisk'}		
        ])
    });
	
	var tpl = new Ext.XTemplate(
		'<tpl for=".">',
            '<div class="thumb-wrap" id="{id}">',
		    '<div class="{rankingColor}"><img src="server.png" title="{name}"></div>',
		    '<span class="x-editable"><b>{shortName}</b></span>',
		    '<span>{shortIpAddrs}</span>',
		    '<span>{status}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
	);

    var dvInstanceList = new Ext.DataView({
		id:'dvInstanceList',				
		store: store,
		tpl: tpl,
		autoScroll  : true,
		singleSelect: true,
		overClass:'x-view-over',
		itemSelector:'div.thumb-wrap',
		emptyText: 'No instances to display',
		prepareData: function(data){
			data.shortName = Ext.util.Format.ellipsis(data.name, 15);
			data.shortIpAddrs = Ext.util.Format.ellipsis(data.ipaddrs, 15);		
			data.rankingColor = 'thumb_'+data.severity;
			//if (data.severity == '1') countSeverity1++; //ijo
			//if (data.severity == '2') countSeverity2++; //kunin
			//if (data.severity == '3') countSeverity3++; //meroh
			return data;
		},
		
		listeners: {			
			selectionchange: {
				fn: function(dv,nodes){
					if (nodes.length > 0){
						glbResourceId = nodes[0].id;
						var selInstance = store.getById(nodes[0].id);
						viewport.get('perf_metrics').setTitle('<center>'+
							'Performance Metrics for '+selInstance.json.name+' ['+selInstance.json.ipaddrs+']</center>');
						glbSelInstanceName = selInstance.json.name;
						loadMask.show();
						//########## set CPU speedometer #########
						var cUtil = CpuUtilanel.getComponent('Cpu_Util_Chart_Panel').getComponent('cpu_utilchart');
						CpuUtilanel.setTitle('<center>CPU Usage ('+selInstance.json.cpuutil+'%)</center>');
						cUtil.chart.setTitle({ text: 'Timestamp: '+selInstance.json.cpudt});
						cUtil.chart.setTitle(null, { text: 'CPU : '+selInstance.json.cputot+' Core'});
						cUtil.chart.series[0].points[0].update(parseFloat(selInstance.json.cpuutil));

						//########## set Memory speedometer #########
						var mUtil = MemoryUtilPanel.getComponent('Mem_Util_Chart_Panel').getComponent('mem_utilchart');
						MemoryUtilPanel.setTitle('<center>Memory Usage ('+selInstance.json.memutil+'%)</center>');
						mUtil.chart.setTitle({ text: 'Timestamp: '+selInstance.json.memdt});
						mUtil.chart.setTitle(null, { text: 'Total Memory : '+selInstance.json.memtot});
						mUtil.chart.series[0].points[0].update(parseFloat(selInstance.json.memutil));

						//########## set Disk speedometer #########
						var sUtil = StorageUtilPanel.getComponent('Storage_Util_Chart_Panel').getComponent('storage_utilchart');
						StorageUtilPanel.setTitle('<center>Disk Usage ('+selInstance.json.diskutil+'%)</center>');
						sUtil.chart.setTitle({ text: 'Timestamp: '+selInstance.json.diskdt});
						sUtil.chart.setTitle(null, { text: 'Total Disk : '+selInstance.json.disktot});
						sUtil.chart.series[0].points[0].update(parseFloat(selInstance.json.diskutil));

						Ext.getCmp('ofname').setValue(selInstance.json.flavorname);
						Ext.getCmp('ofcpu').setValue(selInstance.json.flavorcpu);
						Ext.getCmp('ofram').setValue(selInstance.json.flavormem);
						Ext.getCmp('ofdisk').setValue(selInstance.json.flavordisk);

						/*
						Ext.Ajax.request({   
							url: '../../php/getcpuutil.php',
							params: {
								resourceid: nodes[0].id,
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								var cUtil = CpuUtilanel.getComponent('Cpu_Util_Chart_Panel').getComponent('cpu_utilchart');
								CpuUtilanel.setTitle('<center>CPU Usage ('+json.cpuutil+'%)</center>');
								cUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
								cUtil.chart.setTitle(null, { text: 'CPU : '+json.vcpus+' Core'});
								cUtil.chart.series[0].points[0].update(parseFloat(json.cpuutil));
							}
						});
						Ext.Ajax.request({   
							url: '../../php/getmemutil.php',
							params: {
								resourceid: nodes[0].id,
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								if (json.is_session_expired)
									parent.postMessage(
										'session_expired',
										serverURL
									);
								else{
									var mUtil = MemoryUtilPanel.getComponent('Mem_Util_Chart_Panel').getComponent('mem_utilchart');
									MemoryUtilPanel.setTitle('<center>Memory Usage ('+json.memutil+'%)</center>');
									mUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
									mUtil.chart.setTitle(null, { text: 'Total Memory : '+json.totmem});
									mUtil.chart.series[0].points[0].update(parseFloat(json.memutil));
								}
							}
						});
						Ext.Ajax.request({   
							url: '../../php/getdiskutil.php',
							params: {
								resourceid: nodes[0].id,
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								var sUtil = StorageUtilPanel.getComponent('Storage_Util_Chart_Panel').getComponent('storage_utilchart');
								StorageUtilPanel.setTitle('<center>Disk Usage ('+json.diskutil+'%)</center>');
								//StorageUtilPanel.getTopToolbar().findById('dTimestamp').setText('&nbsp;Timestamp: '+json.timestamp);
								sUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
								sUtil.chart.setTitle(null, { text: 'Total Disk : '+json.totdisk});
								sUtil.chart.series[0].points[0].update(parseFloat(json.diskutil));
							}
						});
						*/
						Ext.Ajax.request({   
							url: './../../php/getdrdw.php',
							params: {
								resourceid: nodes[0].id,
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								if(DrDwUtilPanel.getComponent('Drdw_Util_Chart_Panel'))
				                                DrDwUtilPanel.remove('Drdw_Util_Chart_Panel');
								drawDrDwUtilChart(json);
							}
						}); //end request
						InstanceListPanel.getTopToolbar().findById('lblNormal').setText('Normal ('+countSeverity1+')');
						InstanceListPanel.getTopToolbar().findById('lblCritical').setText('Critical ('+countSeverity3+')');
						InstanceListPanel.getTopToolbar().findById('lblMajor').setText('Major ('+countSeverity2+')');
					}	
				}
			}
		}
	});

    
	var CpuUtilanel = new Ext.Panel({
		id:'CpuUtilanel',
		layout: 'anchor',
		title:'<center>CPU Usage</center>',
        	cls:'home-panel',
		height:339,
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){
					parent.postMessage(
						'cpuutil_history|'+dataParam['projid']+
						'|'+glbResourceId+'|'+glbSelInstanceName,
						serverURL
					);
				}
                     }]        
		})
	});
	
	function drawCpuUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'cpu_utilchart',
			series: [{
				name: 'CPU Utilization',
				data: [80],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		cpu_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Cpu_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 100%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('cpu_utilchart').setWidth(aw);
										Ext.getCmp('cpu_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				cpu_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('cpuUtilChartPanel', cpu_utilchartPanel); 
		CpuUtilanel.add({xtype: 'cpuUtilChartPanel'});
		CpuUtilanel.doLayout();
	}; //end of drawCpuUtilChart

	drawCpuUtilChart();
	
	var MemoryUtilPanel = new Ext.Panel({
		id:'MemoryUtilPanel',
		layout: 'anchor',
		title:'<center>Memory Usage</center>',
        	cls:'home-panel',
		height:339,
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){
					parent.postMessage(
						'memutil_history|'+dataParam['projid']+
						'|'+glbResourceId+'|'+glbSelInstanceName,
						serverURL
					);
				}
                     }]        
		})
	});
	
	function drawMemoryUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'mem_utilchart',
			series: [{
				name: 'Memory Utilization',
				data: [40],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		mem_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Mem_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 100%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('mem_utilchart').setWidth(aw);
										Ext.getCmp('mem_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				mem_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('memUtilChartPanel', mem_utilchartPanel); 
		MemoryUtilPanel.add({xtype: 'memUtilChartPanel'});
		MemoryUtilPanel.doLayout();
	}; //end of drawMemoryUtilChart

	drawMemoryUtilChart();
	
	var StorageUtilPanel = new Ext.Panel({
		id:'StorageUtilPanel',
		layout: 'anchor',
		title:'<center>Disk Usage</center>',
        cls:'home-panel',
		height:339,
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){
					parent.postMessage(
						'diskutil_history|'+dataParam['projid']+
						'|'+glbResourceId+'|'+glbSelInstanceName,
						serverURL
					);
				}
                     }]        
		})
	});
	
	function drawStorageUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'storage_utilchart',
			series: [{
				name: 'Storage Utilization',
				data: [20],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		storage_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Storage_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 100%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('storage_utilchart').setWidth(aw);
										Ext.getCmp('storage_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				storage_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('storageUtilChartPanel', storage_utilchartPanel); 
		StorageUtilPanel.add({xtype: 'storageUtilChartPanel'});
		StorageUtilPanel.doLayout();
	}; //end of drawStorageUtilChart

	drawStorageUtilChart();
	
	var DrDwUtilPanel = new Ext.Panel({
		id:'DrDwUtilPanel',
		layout: 'anchor',
		title:'<center>Disk Read and Write Utilization</center>',
        cls:'home-panel',
		flex:1,
		height:339,
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
							cls: 'custombtn',
                            iconCls: 'speedohistory',height:34,
                            handler: function(){}
                     	}
					]        
		})
	});
	
	function drawDrDwUtilChart(arrChartSeries){
		var chart = new Ext.ux.HighChart({
                id: 'drdw_utilchart',
                series: arrChartSeries,
                chartConfig: {
                    chart: {
                        defaultSeriesType: 'line',
                        zoomType: 'x',
						marginRight: 20
                    },
                    title: { 
						text: 'Last 8 hours Disk Read and Write Utilization (Hourly)',
						style: {
							font: 'normal 12px verdana'
						}
					},
                    credits: {
                            enabled: false
                    },
                    xAxis: {
                        title: {
                            enabled: true,
                            text: 'Date Time'
                        },
                        type: 'datetime',
                        dateTimeLabelFormats : {
                            hour: '%I %p',
                            minute: '%I:%M %p'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Bytes'
                        },
                        labels: {
                            style: {
                                font: 'normal 10px Verdana, sans-serif'
                            },
                            formatter: function() {
                                //return Highcharts.numberFormat(this.value, 0);
                                return Highcharts.numberFormat(this.value, 2); //dua titik perpuluhan
                            }
                        },
                        lineWidth: 1
                    },tooltip: {
                        valueSuffix: ' bytes',
                        crosshairs: true,
                        shared: true
                    },
                    legend:{
                        enabled:true
                    },
                    exporting: {
						buttons:{
							exportButton:{
								enabled: false
							 },
							 printButton: {
								 enabled: false
							 }
						  }
					},					
                    plotOptions: {
                        area: {
                            shadow: false,
                            threshold: null
                        }
                    }
                }
        });
		drdw_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Drdw_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 100%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('drdw_utilchart').setWidth(aw);
										Ext.getCmp('drdw_utilchart').setHeight(260);
									}
								},
								items: [chart]//end items
							}, config);
	
				drdw_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('drdwUtilChartPanel', drdw_utilchartPanel); 
		DrDwUtilPanel.add({xtype: 'drdwUtilChartPanel'});
		DrDwUtilPanel.doLayout();
	}; //end of drawDrDwUtilChart

	//drawDrDwUtilChart2();
	
	
	var InstanceSpeedoPanel = new Ext.Panel({
		id:'InstanceUtilPanel',
		style: 'background-color: #f1f1f1',
		border:false,
		layout:'column',
		flex:1,
		width:750,
		height:500,
        autoScroll:true,
		items:[
			{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0px 5px 5px',
				items:CpuUtilanel
			},{
				columnWidth:.33, 
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0 5px 5px',
				items:MemoryUtilPanel
			},{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0 5px 5px',
				items:StorageUtilPanel
			}]
	});
		  
	var InstanceListPanel = new Ext.Panel({
        id:'images-view',
        anchor:'50% 30%',
        layout:'fit',
        //title:'<center>Instance Summary</center>',
		cls:'home-panel',
		tbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						new Ext.Spacer({width:5,height:20}),new Ext.BoxComponent({
							autoEl: {
								cls: 'critical'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),
						{xtype:'tbtext', id:'lblCritical',
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Critical'
						},new Ext.Spacer({width:5}),'-',new Ext.Spacer({width:5}),new Ext.BoxComponent({
							autoEl: {
								cls: 'major'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),
						{xtype:'tbtext', id:'lblMajor',
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Major'
						},new Ext.Spacer({width:5}),'-',new Ext.Spacer({width:5}),new Ext.BoxComponent({
							autoEl: {
								cls: 'normal'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),
						{xtype:'tbtext', id:'lblNormal', 
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Normal'
						},new Ext.Spacer({width:5}),'-',new Ext.Spacer({width:3}),{  
							text: '&nbsp;Resize Instance&nbsp;',
								iconCls: 'resize',
								cls:'custombtn',
								handler: function(){
									flavorCombo.setValue('');
									Ext.getCmp('nfcpu').setValue('');
									Ext.getCmp('nfram').setValue('');
									Ext.getCmp('nfdisk').setValue('');
									winScaleOut.setTitle("&nbsp;Please select a new flavor to resize "+glbSelInstanceName);
									winScaleOut.show();
								}
						 },'->',{  
							text: '&nbsp;Refresh&nbsp;',
								iconCls: 'refresh',
								cls:'custombtn',
								handler: function(){
									countSeverity1 = 0;
									countSeverity2 = 0;
									countSeverity3 = 0;
									loadMask.show();									
									dvInstanceList.store.reload();
									dvInstanceList.refresh();									
								}
						 },new Ext.Spacer({width:2}),'-',new Ext.Spacer({width:3}),
							{xtype:'tbtext', id:'lblLastRefreshed', 
						   	style:'color:#3e3333;font:normal 12px verdana;', 
						   	text:'Last Refreshed : -'
						}
					]        
		}),
        items: dvInstanceList
    });
	
	  
	var viewport = new Ext.Viewport({
			layout:'border',

			items: [{
				id:'instance_list',
				region:'north',
				baseCls:'x-plain',
				split:true,
				height:160,
				minHeight: 160,
				maxHeight: 200,
				layout:'fit',
				margins: '5 5 0 5',
				items: InstanceListPanel
		   },{
				id:'perf_metrics',
				region:'center',
				margins: '0 5 5 5',
				layout:'anchor',
				title:'<center>Performance Metrics</center>',
				cls:'home-panel',
				items:[{
					anchor:'100%',
					baseCls:'x-plain',
					layout:'hbox',
					layoutConfig: {
						padding:'5',
						align:'top'
					},
					defaults:{margins:'0 0 0 0'},
					items:[InstanceSpeedoPanel,
					DrDwUtilPanel
					]
                }]
          }]
	});  
	
});