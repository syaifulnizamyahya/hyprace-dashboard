Ext.onReady(function(){
	var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});
	var getParams = document.URL.split("/");
	var serverURL = getParams[0]+"//"+getParams[2];
	var splitDataParams = document.URL.split("?");
	var dataParam = Ext.urlDecode(splitDataParams[splitDataParams.length - 1]);
	var store = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../php/getinstancelist.php', 
            method: 'POST',
            headers: {},
            timeout: 60000
        }),
		baseParams: {
			projid: dataParam['projid']
		},
		listeners: {
			load: {
            		fn: function(store, records, options ){
						if (store.reader.jsonData.is_session_expired)
							parent.postMessage(
								'session_expired',
								serverURL
							);
						else
							dvInstanceList.select(0);
            		}
            	}
		},
        autoLoad: true,
        reader: new Ext.data.JsonReader( 
        {
            root: 'images'
        },[   
	     	{name: 'id'},	
            {name: 'name'},
            {name: 'ipaddrs'},
    		{name: 'status'},
			{name: 'severity'}
        ])
    });
	
	var tpl = new Ext.XTemplate(
		'<tpl for=".">',
            '<div class="thumb-wrap" id="{id}">',
		    '<div class="{rankingColor}"><img src="server.png" title="{name}"></div>',
		    '<span class="x-editable"><b>{shortName}</b></span>',
		    '<span>{shortIpAddrs}</span>',
		    '<span>{status}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
	);
   
   	var countSeverity1 = 0;
	var countSeverity2 = 0;
	var countSeverity3 = 0;
    var dvInstanceList = new Ext.DataView({
		id:'dvInstanceList',				
		store: store,
		tpl: tpl,
		autoScroll  : true,
		singleSelect: true,
		overClass:'x-view-over',
		itemSelector:'div.thumb-wrap',
		emptyText: 'No instances to display',
		prepareData: function(data){
			data.shortName = Ext.util.Format.ellipsis(data.name, 15);
			data.shortIpAddrs = Ext.util.Format.ellipsis(data.ipaddrs, 15);
			data.rankingColor = 'thumb_'+data.severity;
			if (data.severity == '1') countSeverity1++; //ijo
			if (data.severity == '2') countSeverity2++;
			if (data.severity == '3') countSeverity3++;
			return data;
		},
		
		listeners: {
			selectionchange: {
				fn: function(dv,nodes){
					if (nodes[0].id){
						var selInstance = store.getById(nodes[0].id);
						viewport.get('perf_metrics').setTitle('<center>'+
							'Performance Metrics for '+selInstance.json.name+' ['+selInstance.json.ipaddrs+']</center>');
						loadMask.show();
						Ext.Ajax.request({   
							url: '../../php/getcpuutil.php',
							params: {
								resourceid: nodes[0].id,
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								var cUtil = CpuUtilanel.getComponent('Cpu_Util_Chart_Panel').getComponent('cpu_utilchart');
								CpuUtilanel.setTitle('<center>CPU Usage ('+json.cpuutil+'%)</center>');
								cUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
								cUtil.chart.setTitle(null, { text: 'CPU : '+json.vcpus+' Core'});
								cUtil.chart.series[0].points[0].update(parseFloat(json.cpuutil));
							}
						});
						Ext.Ajax.request({   
							url: '../../php/getmemutil.php',
							params: {
								resourceid: nodes[0].id,
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								if (json.is_session_expired)
									parent.postMessage(
										'session_expired',
										serverURL
									);
								else{
									var mUtil = MemoryUtilPanel.getComponent('Mem_Util_Chart_Panel').getComponent('mem_utilchart');
									MemoryUtilPanel.setTitle('<center>Memory Usage ('+json.memutil+'%)</center>');
									mUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
									mUtil.chart.setTitle(null, { text: 'Total Memory : '+json.totmem});
									mUtil.chart.series[0].points[0].update(parseFloat(json.memutil));
								}
							}
						});
						Ext.Ajax.request({   
							url: '../../php/getdiskutil.php',
							params: {
								resourceid: nodes[0].id,
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								loadMask.hide();
								var json = Ext.util.JSON.decode(response.responseText);
								var sUtil = StorageUtilPanel.getComponent('Storage_Util_Chart_Panel').getComponent('storage_utilchart');
								StorageUtilPanel.setTitle('<center>Disk Usage ('+json.diskutil+'%)</center>');
								//StorageUtilPanel.getTopToolbar().findById('dTimestamp').setText('&nbsp;Timestamp: '+json.timestamp);
								sUtil.chart.setTitle({ text: 'Timestamp: '+json.timestamp});
								sUtil.chart.setTitle(null, { text: 'Total Disk : '+json.totdisk});
								sUtil.chart.series[0].points[0].update(parseFloat(json.diskutil));
							}
						});
						Ext.Ajax.request({   
							url: './../../php/gettraffic.php',
							params: {
								resourceid: nodes[0].id,
								numofinstance: store.getCount(),
								projid: dataParam['projid']
							},
							callback: function (options, success, response) {
								var json = Ext.util.JSON.decode(response.responseText);
								//console.dir(json);
								if(TrafficUtilPanel.getComponent('Traffic_Util_Chart_Panel'))
			                                		TrafficUtilPanel.remove('Traffic_Util_Chart_Panel');
								drawTrafficUtilChart(json);	
							}
						}); //end request
						InstanceListPanel.getTopToolbar().findById('lblNormal').setText('Normal ('+countSeverity1+')');
						InstanceListPanel.getTopToolbar().findById('lblCritical').setText('Critical ('+countSeverity3+')');
						InstanceListPanel.getTopToolbar().findById('lblMajor').setText('Major ('+countSeverity2+')');
					}	
				}
			}
		}
	});

    
	var CpuUtilanel = new Ext.Panel({
		id:'CpuUtilanel',
		layout: 'anchor',
		title:'<center>CPU Usage</center>',
        	cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){}
                     }]        
		})
	});
	
	function drawCpuUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'cpu_utilchart',
			series: [{
				name: 'CPU Utilization',
				data: [80],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		cpu_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Cpu_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 82%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('cpu_utilchart').setWidth(aw);
										Ext.getCmp('cpu_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				cpu_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('cpuUtilChartPanel', cpu_utilchartPanel); 
		CpuUtilanel.add({xtype: 'cpuUtilChartPanel'});
		CpuUtilanel.doLayout();
	}; //end of drawCpuUtilChart

	drawCpuUtilChart();
	
	var MemoryUtilPanel = new Ext.Panel({
		id:'MemoryUtilPanel',
		layout: 'anchor',
		title:'<center>Memory Usage</center>',
        	cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
				cls:'custombtn',
                            handler: function(){}
                     }]        
		})
	});
	
	function drawMemoryUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'mem_utilchart',
			series: [{
				name: 'Memory Utilization',
				data: [40],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		mem_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Mem_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 82%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('mem_utilchart').setWidth(aw);
										Ext.getCmp('mem_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				mem_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('memUtilChartPanel', mem_utilchartPanel); 
		MemoryUtilPanel.add({xtype: 'memUtilChartPanel'});
		MemoryUtilPanel.doLayout();
	}; //end of drawMemoryUtilChart

	drawMemoryUtilChart();
	
	var StorageUtilPanel = new Ext.Panel({
		id:'StorageUtilPanel',
		layout: 'anchor',
		title:'<center>Disk Usage</center>',
        cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
							cls:'custombtn',
                            handler: function(){}
                     }]        
		})
	});
	
	function drawStorageUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'storage_utilchart',
			series: [{
				name: 'Storage Utilization',
				data: [20],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' ',
					style: {
						font: 'normal 12px verdana'
					}
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 180,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		storage_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Storage_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 82%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('storage_utilchart').setWidth(aw);
										Ext.getCmp('storage_utilchart').setHeight(400);
									}
								},
								items: [chart]//end items
							}, config);
	
				storage_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('storageUtilChartPanel', storage_utilchartPanel); 
		StorageUtilPanel.add({xtype: 'storageUtilChartPanel'});
		StorageUtilPanel.doLayout();
	}; //end of drawStorageUtilChart

	drawStorageUtilChart();
	
	var TrafficUtilPanel = new Ext.Panel({
		id:'TrafficUtilPanel',
		layout: 'anchor',
		title:'<center>Incoming And Outgoing Traffic Usage</center>',
        cls:'home-panel',
		flex:1,
		height:336,
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						{  
                     	text: '&nbsp;View History&nbsp;',
							cls: 'custombtn',
                            iconCls: 'speedohistory',height:34,
                            handler: function(){}
                     	}
					]        
		})
	});
	
	function drawTrafficUtilChart(arrChartSeries){
		var chart = new Ext.ux.HighChart({
                id: 'traffic_utilchart',
                series: arrChartSeries,
                chartConfig: {
                    chart: {
                        defaultSeriesType: 'line',
                        zoomType: 'x',
						marginRight: 20
                    },
                    title: { 
						text: 'Last 8 hours Incoming and Outgoing traffic (Hourly)',
						style: {
							font: 'normal 12px verdana'
						}
					},
                    credits: {
                            enabled: false
                    },
                    xAxis: {
                        title: {
                            enabled: true,
                            text: 'Date Time'
                        },
                        type: 'datetime',
                        dateTimeLabelFormats : {
                            hour: '%I %p',
                            minute: '%I:%M %p'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Bytes'
                        },
                        labels: {
                            style: {
                                font: 'normal 10px Verdana, sans-serif'
                            },
                            formatter: function() {
                                //return Highcharts.numberFormat(this.value, 0);
                                return Highcharts.numberFormat(this.value, 2); //dua titik perpuluhan
                            }
                        },
                        lineWidth: 1
                    },tooltip: {
                        valueSuffix: ' bytes',
                        crosshairs: true,
                        shared: true
                    },
                    legend:{
                        enabled:true
                    },
                    exporting: {
						buttons:{
							exportButton:{
								enabled: false
							 },
							 printButton: {
								 enabled: false
							 }
						  }
					},					
                    plotOptions: {
                        area: {
                            shadow: false,
                            threshold: null
                        }
                    }
                }
        });
		traffic_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Traffic_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 100%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('traffic_utilchart').setWidth(aw);
										Ext.getCmp('traffic_utilchart').setHeight(260);
									}
								},
								items: [chart]//end items
							}, config);
	
				traffic_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('trafficUtilChartPanel', traffic_utilchartPanel); 
		TrafficUtilPanel.add({xtype: 'trafficUtilChartPanel'});
		TrafficUtilPanel.doLayout();
	}; //end of drawTrafficUtilChart

	//drawTrafficUtilChart2();
	
	
	var InstanceSpeedoPanel = new Ext.Panel({
		id:'InstanceUtilPanel',
		style: 'background-color: #f1f1f1',
		border:false,
		layout:'column',
		flex:1,
		width:750,
		height:500,
        autoScroll:true,
		items:[
			{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0px 5px 5px',
				items:CpuUtilanel
			},{
				columnWidth:.33, 
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0 5px 5px',
				items:MemoryUtilPanel
			},{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:0px 0 5px 5px',
				items:StorageUtilPanel
			}]
	});
		  
	var InstanceListPanel = new Ext.Panel({
        id:'images-view',
        anchor:'50% 30%',
        layout:'fit',
        //title:'<center>Instance Summary</center>',
		cls:'home-panel',
		tbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [
						new Ext.Spacer({width:5,height:20}),new Ext.BoxComponent({
							autoEl: {
								cls: 'critical'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),
						{xtype:'tbtext', id:'lblCritical',
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Critical'
						},new Ext.Spacer({width:5}),'-',new Ext.Spacer({width:5}),new Ext.BoxComponent({
							autoEl: {
								cls: 'major'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),
						{xtype:'tbtext', id:'lblMajor',
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Major'
						},new Ext.Spacer({width:5}),'-',new Ext.Spacer({width:5}),new Ext.BoxComponent({
							autoEl: {
								cls: 'normal'
							},
							height:14,
							width:15
						}),new Ext.Spacer({width:3}),
						{xtype:'tbtext', id:'lblNormal', 
						   style:'color:#3e3333;font:normal 12px verdana;', 
						   text:'Normal'
						}
					]        
		}),
        items: dvInstanceList
    });
	
	  
	var viewport = new Ext.Viewport({
			layout:'border',

			items: [{
				id:'instance_list',
				region:'north',
				baseCls:'x-plain',
				split:true,
				height:160,
				minHeight: 160,
				maxHeight: 200,
				layout:'fit',
				margins: '5 5 0 5',
				items: InstanceListPanel
		   },{
				id:'perf_metrics',
				region:'center',
				margins: '0 5 5 5',
				layout:'anchor',
				autoScroll : true,
				title:'<center>Performance Metrics</center>',
				cls:'home-panel',
				items:[{
					anchor:'100%',
					baseCls:'x-plain',
					layout:'hbox',
					layoutConfig: {
						padding:'5',
						align:'top'
					},
					defaults:{margins:'0 5 0 0'},
					items:[InstanceSpeedoPanel,
					TrafficUtilPanel
					]
                }]
          }]
	});  
	
});