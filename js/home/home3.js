Ext.onReady(function(){
	var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});
	var getParams = document.URL.split("/");
	var serverURL = getParams[0]+"//"+getParams[2];
	var store = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../php/getinstancelist.php', 
            method: 'POST',
            headers: {},
            timeout: 60000
        }),
        autoLoad: true,
        reader: new Ext.data.JsonReader( 
        {
            root: 'images'
        },[   
	     {name: 'id'},
            {name: 'name'},
            {name: 'ipaddrs'},
	     {name: 'status'}
        ])
    });

    var tpl = new Ext.XTemplate(
		'<tpl for=".">',
            '<div class="thumb-wrap" id="{id}">',
		    '<div class="thumb"><img src="server.png" title="{name}"></div>',
		    '<span class="x-editable"><b>{shortName}</b></span>',
		    '<span>{ipaddrs}</span>',
		    '<span>{status}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
	);

    var panel = new Ext.Panel({
        id:'images-view',
        frame:true,
		//anchor:'90%',
        //height:250,
		//width:300,
                    anchor:'50% 30%',
        layout:'fit',
        title:'<center>Instances</center>',
		cls:'home-panel',
        items: new Ext.DataView({
            id:'dvv',
	     store: store,
            tpl: tpl,
	     autoScroll  : true,
            multiSelect: true,
            overClass:'x-view-over',
            itemSelector:'div.thumb-wrap',
            emptyText: 'No instances to display',
            prepareData: function(data){
                data.shortName = Ext.util.Format.ellipsis(data.name, 15);
                //data.sizeString = Ext.util.Format.fileSize(data.size);
//                data.dateString = data.lastmod.format("m/d/Y g:i a");
                return data;
            },
            
            listeners: {
            	selectionchange: {
            		fn: function(dv,nodes){
				if (nodes[0].id){
					var selInstance = store.getById(nodes[0].id);
					loadMask.show();
					Ext.Ajax.request({   
						url: '../../php/getcpuutil.php',
						params: {
							resourceid: nodes[0].id
						},
						callback: function (options, success, response) {
							loadMask.hide();
							var json = Ext.util.JSON.decode(response.responseText);
							var cUtil = CpuUtilanel.getComponent('Cpu_Util_Chart_Panel').getComponent('cpu_utilchart');
							cUtil.chart.setTitle({ text: selInstance.json.name+' CPU Utilization ('+json.cpuutil+'%)',
										  style: { color: 'red',font:'normal 12px verdana' }});
							//cUtil.chart.setTitle(null, { text: 'Timestamp : '+json.timestamp});
							
							cUtil.chart.series[0].points[0].update(parseFloat(json.cpuutil));
						}
					});
					Ext.Ajax.request({   
						url: '../../php/getmemutil.php',
						params: {
							resourceid: nodes[0].id
						},
						callback: function (options, success, response) {
							loadMask.hide();
							var json = Ext.util.JSON.decode(response.responseText);
							if (json.is_session_expired)
								parent.postMessage(
									'session_expired',
									serverURL
								);
							else{
								var mUtil = MemoryUtilPanel.getComponent('Mem_Util_Chart_Panel').getComponent('mem_utilchart');
								mUtil.chart.setTitle({ text: selInstance.json.name+' Memory Utilization ('+json.memutil+'%)'});
								mUtil.chart.setTitle(null, { text: 'Timestamp : '+json.timestamp});
								mUtil.chart.series[0].points[0].update(parseFloat(json.memutil));
							}
						}
					});
					Ext.Ajax.request({   
						url: '../../php/getdiskutil.php',
						params: {
							resourceid: nodes[0].id
						},
						callback: function (options, success, response) {
							loadMask.hide();
							var json = Ext.util.JSON.decode(response.responseText);
							var sUtil = StorageUtilPanel.getComponent('Storage_Util_Chart_Panel').getComponent('storage_utilchart');
							sUtil.chart.setTitle({ text: selInstance.json.name+' Disk Utilization ('+json.diskutil+'%)'});
							sUtil.chart.setTitle(null, { text: 'Timestamp : '+json.timestamp});
							console.info(parseFloat(json.diskutil));
							sUtil.chart.series[0].points[0].update(parseFloat(json.diskutil));
							
						}
					});
				}
            		}
            	}
            }
        })
    });

    	panel.render('hyprace_instance_list');
	panel.get('dvv').store.on('load',function(store,records,options){
		if (store.reader.jsonData.is_session_expired)
			parent.postMessage(
				'session_expired',
				serverURL
			);
		else
			panel.get('dvv').select(0);
	},this);
	
	var CpuUtilanel = new Ext.Panel({
		id:'CpuUtilanel',
		layout: 'anchor',
		title:'<center>CPU Utilization</center>',
        	cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
                            handler: function(){}
                     }]        
		})
	});
	
	function drawCpuUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'cpu_utilchart',
			series: [{
				name: 'CPU Utilization',
				data: [80],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					//renderTo: 'container',
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},				
				title: {
					text: ' '
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            				size: 200,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		cpu_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Cpu_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 80%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('cpu_utilchart').setWidth(aw);
										Ext.getCmp('cpu_utilchart').setHeight(550);
									}
								},
								items: [chart]//end items
							}, config);
	
				cpu_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('cpuUtilChartPanel', cpu_utilchartPanel); 
		CpuUtilanel.add({xtype: 'cpuUtilChartPanel'});
		CpuUtilanel.doLayout();
	}; //end of drawCpuUtilChart

	drawCpuUtilChart();
	
	var MemoryUtilPanel = new Ext.Panel({
		id:'MemoryUtilPanel',
		layout: 'anchor',
		title:'<center>Memory Utilization</center>',
        	cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
                            handler: function(){}
                     }]        
		})
	});
	
	function drawMemoryUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'mem_utilchart',
			series: [{
				name: 'Memory Utilization',
				data: [40],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},				
				title: {
					text: ' '
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            		size: 200,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		mem_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Mem_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 80%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('mem_utilchart').setWidth(aw);
										Ext.getCmp('mem_utilchart').setHeight(550);
									}
								},
								items: [chart]//end items
							}, config);
	
				mem_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('memUtilChartPanel', mem_utilchartPanel); 
		MemoryUtilPanel.add({xtype: 'memUtilChartPanel'});
		MemoryUtilPanel.doLayout();
	}; //end of drawMemoryUtilChart

	drawMemoryUtilChart();
	
	var StorageUtilPanel = new Ext.Panel({
		id:'StorageUtilPanel',
		layout: 'anchor',
		title:'<center>Disk Utilization</center>',
        	cls:'home-panel',
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [{  
                     	text: '&nbsp;View History&nbsp;',
                            iconCls: 'speedohistory',height:34,
                            handler: function(){}
                     }]        
		})
	});
	
	function drawStorageUtilChart(){
		var chart = new Ext.ux.HighChart({
			id: 'storage_utilchart',
			series: [{
				name: 'Disk Utilization',
				data: [20],
				tooltip: {
					valueSuffix: ' %'
				}
			}],
			chartConfig: {
				chart: {
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				title: {
					text: ' '
				},
				subtitle: {
					text: ' '
				},
				credits: {
					enabled: false
				},
				pane: {
					startAngle: -150,
					endAngle: 150,
					center: ['50%', '30%'],
            		size: 200,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				exporting: {
                        buttons:{
                            exportButton:{
                                enabled: false
                             },
                             printButton: {
                                 enabled: false
                             }
                          }
				},   
				// the value axis
				yAxis: {
					min: 0,
					max: 100,
					
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
			
					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: '%'
					},
					plotBands: [{
						from: 0,
						to: 50,
						color: '#55BF3B' // green
					}, {
						from: 50,
						to: 80,
						color: '#DDDF0D' // yellow
					}, {
						from: 80,
						to: 100,
						color: '#DF5353' // red
					}]        
				}}
        });
		storage_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Storage_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 80%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('storage_utilchart').setWidth(aw);
										Ext.getCmp('storage_utilchart').setHeight(550);
									}
								},
								items: [chart]//end items
							}, config);
	
				storage_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('storageUtilChartPanel', storage_utilchartPanel); 
		StorageUtilPanel.add({xtype: 'storageUtilChartPanel'});
		StorageUtilPanel.doLayout();
	}; //end of drawStorageUtilChart

	drawStorageUtilChart();
	
	var InstanceUtilPanel = new Ext.Panel({
		id:'InstanceUtilPanel',
		//style: 'background-color: #f1f1f1',
		baseCls:'x-plain',
		//margins: '5 5 5 0',
		renderTo : 'hyprace_instance_perf',
		border:false,
		layout:'column',
        autoScroll:true,
		items:[
			{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:5px 0px 5px 5px',
				items:CpuUtilanel
			},{
				columnWidth:.33, 
				baseCls:'x-plain',
				bodyStyle:'padding:5px 0 5px 5px',
				items:MemoryUtilPanel
			},{
				columnWidth:.33,
				baseCls:'x-plain',
				bodyStyle:'padding:5px 5px 5px 5px',
				items:StorageUtilPanel
			}]
	});
	
});