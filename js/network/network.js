Ext.onReady(function(){
	var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});
	var getParams = document.URL.split("/");
	var serverURL = getParams[0]+"//"+getParams[2];
	var splitDataParams = document.URL.split("?");
	var dataParam = Ext.urlDecode(splitDataParams[splitDataParams.length - 1]);
	
	//######################## BEGIN : NETWORK LIST #########################
	var storeNetworkList = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            		url: '../../php/getNetworkList.php', 
            		method: 'POST',
            		headers: {},
            		timeout: 60000
       	}),
		baseParams: {
			projid: dataParam['projid']
		},
		listeners: {
			load: {
            		fn: function(store, records, options ){
						if (store.reader.jsonData.is_session_expired)
							parent.postMessage(
								'session_expired',
								serverURL
							);
						else
							dvNetworkList.select(0);
            		}
            	}
		},
        autoLoad: true,
        reader: new Ext.data.JsonReader( 
        {
            root: 'images'
        },[   
	     	{name: 'id'},	
            {name: 'name'},
            {name: 'subnet'},
    		{name: 'status'}
        ])
    });
	
	var tplNt = new Ext.XTemplate(
		'<tpl for=".">',
            '<div class="thumb-wrap" id="{id}">',
		    '<div class="{rankingColor}"><img src="network.png" title="{name}"></div>',
		    '<span class="x-editable"><b>{shortName}</b></span>',
		    '<span>{shortSubnet}</span>',
		    '<span>{status}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
	);
   
    var dvNetworkList = new Ext.DataView({
		id:'dvNetworkList',				
		store: storeNetworkList,
		tpl: tplNt,
		autoScroll  : true,
		singleSelect: true,
		overClass:'x-view-over',
		itemSelector:'div.thumb-wrap',
		emptyText: 'No instances to display',
		prepareData: function(data){
			data.shortName = Ext.util.Format.ellipsis(data.name, 15);
			data.shortSubnet = Ext.util.Format.ellipsis(data.subnet, 15);
			data.rankingColor = 'thumb';
			return data;
		},
		
		listeners: {
			selectionchange: {
				fn: function(dv,nodes){
					if (nodes[0].id){
						var selNetwork = storeNetworkList.getById(nodes[0].id);
						storeInstanceList.setBaseParam('networkname', selNetwork.data.name);
						storeInstanceList.reload();
					}
						
				}
			}
		}
	});
	
	//######################## END : NETWORK LIST #########################

	//######################## BEGIN : INSTANCE LIST #########################
	
	var storeInstanceList = new Ext.data.Store({
	        proxy: new Ext.data.HttpProxy({
            	     url: '../../php/getNetworkInstList.php', 
       	     method: 'POST',
	            headers: {},
            		timeout: 60000
        	}),
		listeners: {
			load: {
            			fn: function(store, records, options ){
					if (store.getTotalCount()>0){
						dvInstanceList.select(0);
						updateGraphTraffic(records[0].id);
					}else
						if(TraffUtilPanel.getComponent('Traff_Util_Chart_Panel'))
							TraffUtilPanel.remove('Traff_Util_Chart_Panel');

            			}
	     		}
		},
        	reader: new Ext.data.JsonReader( 
       	 {
            		root: 'images'
        	 },[   
	     		{name: 'id'},	
            		{name: 'name'},
		       {name: 'ipaddrs'}
        	])
    	});
	
	var tplIns = new Ext.XTemplate(
		'<tpl for=".">',
            	    '<div class="thumb-wrap" id="{id}">',
		    '<div class="thumb"><img src="server.png" title="{name}"></div>',
		    '<span class="x-editable"><b>{shortName}</b></span>',
		    '<span>{ipaddrs}</span>',
		    '<span>{status}</span></div>',
	       '</tpl>',
        	'<div class="x-clear"></div>'
	);
   
    	var dvInstanceList = new Ext.DataView({
		id:'dvInstanceList',				
		store: storeInstanceList,
		tpl: tplIns,
		autoScroll  : true,
		singleSelect: true,
		overClass:'x-view-over',
		itemSelector:'div.thumb-wrap',
		//emptyText: 'No instances to display',
		prepareData: function(data){
			data.shortName = Ext.util.Format.ellipsis(data.name, 15);
			return data;
		},		
		listeners: {
			click : {
				fn: function(dv, index, node, e) {
					updateGraphTraffic(node.id);
				}
			}
		}
	});

	function updateGraphTraffic(nodeId){
		var selInstance = storeInstanceList.getById(nodeId);
		Ext.Ajax.request({   
			url: './../../php/gettraffic.php',
			params: {
				resourceid: selInstance.id,
				projid: dataParam['projid']
			},
			callback: function (options, success, response) {
				var json = Ext.util.JSON.decode(response.responseText);
				
				if(TraffUtilPanel.getComponent('Traff_Util_Chart_Panel'))
					TraffUtilPanel.remove('Traff_Util_Chart_Panel');
				drawTraffUtilChart(json);	
			}
		}); //end request
	}
	
	//######################## END : INSTANCE LIST #########################
	
	var TraffUtilPanel = new Ext.Panel({
		id:'TraffUtilPanel',
		layout: 'anchor',
		title:'<center>Incoming and Outgoing Traffic Utilization</center>',
        	cls:'home-panel',
		flex:1,
		height:339,
		anchor: '100% 50%',
		bbar:new Ext.Toolbar({
            		enableOverflow: true,
	            	items:  [{  
                     	text: '&nbsp;View History&nbsp;',
				cls: 'custombtn',
                            iconCls: 'speedohistory',height:34,
                            handler: function(){}
                     }]        
		})
	});
	
	function drawTraffUtilChart(arrChartSeries){
		var chart = new Ext.ux.HighChart({
                id: 'traff_utilchart',
                series: arrChartSeries,
                chartConfig: {
                    chart: {
                        defaultSeriesType: 'line',
                        zoomType: 'x',
			   marginRight: 20
                    },
                    title: { 
			 text: 'Last 8 hours Incoming and Outgoing Traffic Utilization (Every 10 Minutes interval)',
			 style: {
				font: 'normal 12px verdana'
			 }
		      },
                    credits: {
                            enabled: false
                    },
                    xAxis: {
                        title: {
                            enabled: true,
                            text: 'Date Time'
                        },
                        type: 'datetime',
                        dateTimeLabelFormats : {
                            hour: '%I %p',
                            minute: '%I:%M %p'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Bytes/sec'
                        },
                        labels: {
                            style: {
                                font: 'normal 10px Verdana, sans-serif'
                            },
                            formatter: function() {
                                //return Highcharts.numberFormat(this.value, 0);
                                return Highcharts.numberFormat(this.value, 2); //dua titik perpuluhan
                            }
                        },
                        lineWidth: 1
                    },tooltip: {
                        valueSuffix: ' bytes/sec',
                        crosshairs: true,
                        shared: true
                    },
                    legend:{
                        enabled:true
                    },
                    exporting: {
				buttons:{
					exportButton:{
						enabled: false
					 },
					 printButton: {
						 enabled: false
					 }
				}
		      },					
                    plotOptions: {
                        area: {
                            shadow: false,
                            threshold: null
                        }
                    }
                }
        	});
		traff_utilchartPanel = Ext.extend(Ext.Panel, {
			id:'Traff_Util_Chart_Panel',
			border:true,
			frame:true,
			constructor : function (config) {
				config = config || {};
				var arguments = Ext.apply({
								layout: 'anchor',
								baseCls:'x-plain',
								anchor: '100% 100%',
								listeners: {
									resize : function(component, aw, ah) {
										Ext.getCmp('traff_utilchart').setWidth(aw);
										Ext.getCmp('traff_utilchart').setHeight(260);
									}
								},
								items: [chart]//end items
							}, config);
	
				traff_utilchartPanel.superclass.constructor.call(this, arguments);
			}//end constructor()
		}); 
		Ext.reg('traffUtilChartPanel', traff_utilchartPanel); 
		TraffUtilPanel.add({xtype: 'traffUtilChartPanel'});
		TraffUtilPanel.doLayout();
	}; //end of drawTraffUtilChart

	var NetworkListPanel = new Ext.Panel({
        id:'images-view',
		title:'Network List',
        cls:'home-panel',
        anchor:'50% 30%',
        layout:'fit',
        items: dvNetworkList
    	});
	
	var InstanceListPanel = new Ext.Panel({
        id:'images-view',
		title:'Instance List',
        cls:'home-panel',
        anchor:'50% 30%',
        layout:'fit',
        items: dvInstanceList
    	});
	  
	var viewport = new Ext.Viewport({
			layout:'border',

			items: [{
                region: 'west',
                //collapsible: true,
                baseCls:'x-plain',
				split: true,
                width: 220, // give east and west regions a width
                minSize: 220,
                maxSize: 500,
                margins: '5 0 0 5',
                layout: 'fit', // specify layout manager for items
                items: NetworkListPanel
            },{
				id:'instance_list',
				region:'center',
				baseCls:'x-plain',
				split:true,
				height:180,
				minHeight: 180,
				maxHeight: 200,
				layout:'fit',
				margins: '5 5 0 0',
				items: InstanceListPanel
		   },{
				id:'perf_metrics',
				region:'south',
				margins: '5 5 5 5',
				height:380,
				layout:'anchor',
				//title:'<center>Performance Metrics</center>',
				//cls:'home-panel',
				items:[{
					anchor:'100%',
					baseCls:'x-plain',
					layout:'hbox',
					layoutConfig: {
						padding:'5',
						align:'top'
					},
					defaults:{margins:'0 0 0 0'},
					items:TraffUtilPanel
                }]
          }]
	});  
	
});