
Ext.onReady(function(){
    var getParams = document.URL.split("/");
    var serverURL = getParams[0]+"//"+getParams[2];	
    var TabAccessView = [];
    TabAccessView.push(TabAccessView.length);
    TabAccessView[(TabAccessView.length-1)] = {  
          title:"&nbsp;Instances&nbsp;",
          id:'home_tab',
          iconCls:'instances',
          closable:false,
          defaultSrc:'./js/home/home.html?projid='+glbProjectMenu.projdefaultid
    };		 
	
	//console.info('./js/home/home.html?projid='+glbProjectMenu.projdefaultid);

    TabAccessView.push(TabAccessView.length);
    TabAccessView[(TabAccessView.length-1)] = {  
          title:"&nbsp;Network&nbsp;",
          id:'network_tab',
          iconCls:'network',
          closable:false,
          defaultSrc:'./js/network/network.html?projid='+glbProjectMenu.projdefaultid
    };
	
	
    function listener(event){
     if ( event.origin !== serverURL )
          return	
	 continueNext(event.data);        
    };

    function continueNext(iFramePath){
	var strParam = iFramePath.split("|");
	if (strParam[0] == 'session_expired'){
		exitAppsFinal();
	}else if (strParam[0] == 'cpuutil_history'){
		openNewTab("&nbsp;CPU Usage for "+strParam[3]+"&nbsp;",
			   "cpuusage-"+strParam[2], "chart2", 
			   './js/history/cpu_hist.html?projectid='+strParam[1]+
			   '&resourceid='+strParam[2]+'&resourcename='+strParam[3]);
	}else if (strParam[0] == 'memutil_history'){
		openNewTab("&nbsp;Memory Usage for "+strParam[3]+"&nbsp;",
			   "memusage-"+strParam[2], "chart2", 
			   './js/history/mem_hist.html?projectid='+strParam[1]+
			   '&resourceid='+strParam[2]+'&resourcename='+strParam[3]);
	}else if (strParam[0] == 'diskutil_history'){
		openNewTab("&nbsp;Disk Usage for "+strParam[3]+"&nbsp;",
			   "diskusage-"+strParam[2], "chart2", 
			   './js/history/disk_hist.html?projectid='+strParam[1]+
			   '&resourceid='+strParam[2]+'&resourcename='+strParam[3]);
	}		
    };
	
    if (window.addEventListener){
        addEventListener("message", listener, false)
    } else {
        attachEvent("onmessage", listener)
    };  

    var baselineFrames = new Ext.ux.ManagedIframePanel({
		region:'center',
		id:'baselineFrames',
		autoScroll  : false,
		defaultSrc:'./../hypracemon/js/baseline/baseline.html',
		anchor:"100% -1",
		listeners: {
				domready : function(frame) {
						//alert('dr');
				},
				documentloaded : function(frame) {
						//console.info('DL1:');
				}
		}
    });
	var winBaseline = new Ext.Window({
		title: "&nbsp;Baseline Settings",
		width: 530, 
		height:160, 
		iconCls:'baseline',
		//closable :true,
		closeAction:'hide',
		draggable : false,
		resizable : false,
		x:(Ext.getBody().getWidth()/2)-300,
		y:30,
		border:true,
		plain: true,
		shadow : Ext.isIE?false:true,
		frame:true,
		bodyStyle:'padding:5px;',
		modal:true,
		layout: 'fit',
		items: [baselineFrames]
	});

    var tabs = new Ext.TabPanel({
            xtype:'tabpanel',
            deferredRender:true,
            layoutOnTabChange:true,
            region:'center',
            id:'maintab',
            margins:'1 1 1 1',
            enableTabScroll: true,
			defaults:{
				closable:false,
				loadMask:{hideOnReady :false,msg:'Loading...'},
				autoScroll : true,
				autoShow:true
			  },
            defaultType:"iframepanel",
            activeTab:0,
            //autoHeight:true,
            //autoWidth:true,
            border:false,
            bodyStyle:'background-color: #eaeaea;',
            //height:500,
            items:[TabAccessView]
    });
  
  	var projectMenu = new Ext.menu.Menu({
        id: 'projectMenu',
        items: glbProjectMenu.projectmenu,
		listeners: {
			itemclick: {
            		fn: function(baseItm, e){
						projectMenu.items.each(function(itemss ) { 
							itemss.setIconClass(null);
						});
						baseItm.setIconClass('displaycheck');
						tb_hyprace.findById('menuProject').setText('&nbsp;Project Name : '+baseItm.text+'&nbsp;&nbsp;');
						Ext.getCmp('maintab').activeTab.setSrc('./js/home/home.html?projid='+baseItm.id);
            		}
            	}
		}
    });
  
    var menuBarButton = [
        new Ext.Spacer({width:4}),
	 {
	     id : 'menuProject',
            text: '&nbsp;Project Name : '+glbProjectMenu.projdefaultname+'&nbsp;&nbsp;',
	     iconCls:'project',	
            menu: projectMenu
        },'-',{
            text: '&nbsp;Baseline&nbsp;',
            iconCls: 'baseline',
            handler: function(){
                winBaseline.show();
            }
        },'->',{
            text: '&nbsp;Welcome, '+glbUserId,
            iconCls:'welcome_user'
        },new Ext.Spacer({width:5}),'-',new Ext.Spacer({width:5}),
		{
			text: '&nbsp;Logout&nbsp;',
			handler: function(){
				Ext.MessageBox.confirm('Confirmation','Are you sure you want to exit?', exitAppsDialog);
			},
			iconCls: 'logout'
		}
    ];

	
    var tb_hyprace = new Ext.Toolbar({
            enableOverflow: true,
            items:  menuBarButton        
    });
    
    
    function openNewTab(titTab, idTab, iconcls, src){
        if (idTab == 'report9id')
            tabs.add({
                title: titTab,
                id: idTab,
                iconCls: iconcls,
                defaultSrc: src,
                closable:true,
                tbar:[{
                    text: '&nbsp;Print&nbsp',
                    iconCls:'print',
                    handler: printPanel
                },'-',{
                    text: '&nbsp;Refresh&nbsp;',
                    iconCls: 'Refresh',
                    handler: function(){
                        Ext.getCmp('maintab').activeTab.setSrc();
                    }
                }]
            }).show();
        else    
            tabs.add({
                title: titTab,
                id: idTab,
                iconCls: iconcls,
                defaultSrc: src,
                closable:true
            }).show();
    };
	
    
    function exitAppsDialog(btn){
        if (btn == 'yes') {
            exitAppsFinal();    
        }
    };

    function exitAppsFinal(){
            Ext.Ajax.request({
                url: './logout.php',
                callback: function (options, success, response) {
                    window.location = './index.html';
                }
            }); //end request   
    };

    var viewport = new Ext.Viewport
    ({
        layout : "border",
        items : 
        [{
                region : "north",
                height : 82,
                //margins: '0 15 0 15',
                bodyStyle: "background: none; border: none;",
                contentEl: 'bannerbg',
                bbar: tb_hyprace
         },tabs,
         {
                region : "south",
                //height : 30,
                bodyStyle: "background: #eaf1f6 url(img/botbar.png) repeat center; color: #eaeaea; border: none; text-align: center; padding: 4px;",
                html: '<div id="bottom_bar">Best viewed with Google Chrome. &copy; 2015 Hyprace   <a href="http://www.tmrnd.com.my/" target="_blank">TM Research & Development</a></div>'
         }]
    });

});