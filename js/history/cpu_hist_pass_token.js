

Ext.onReady(function() {
    var getParams = document.URL.split("?");
    var homeParam = Ext.urlDecode(getParams[getParams.length - 1]);
    var strToken = homeParam['tokenid'];
    var strResource = homeParam['resourceid'];
    Ext.Ajax.request({   
            url: './../../php/getCpuHist.php',
	     params: { 
                  tokenid : strToken,
                  resourceid:strResource
            },
            callback: function (options, success, response) {
                    var json = Ext.util.JSON.decode(response.responseText);                    
                    if (json.avgcpu.length == 0) 
			   Ext.MessageBox.show({
			   	title: 'Data not available',
				msg: 'No data found.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.WARNING
			   });
		      addCpuUsageGraph(json.avgcpu,json.range,'CPU Usage (%) from '+json.dtF+' to '+json.dtT);								                    
            }
    }); //end request
    
    var hypraceChartForm = new Ext.Panel({
        id:'chart-panel',
        border:false,
        region:'center',
        frame:false,
        defaults: {anchor: '100%'},		
        layout:'anchor'
//        ,
//        tbar: ['&nbsp;Month :',monthcombo,'&nbsp;','-','&nbsp;Year :',
//            {
//                id:'selyear',
//                xtype: 'numberfield',
//                value: '2013',
//                width:50,
//                name: 'year',
//                allowBlank:false
//            },new Ext.Spacer({width:4}),'-',new Ext.Spacer({width:1}),
//            {
//                text:'&nbsp;View&nbsp;',
//                tooltip: 'View Chart',   
//                iconCls: 'chart',
//                handler: function(){
//                    if (monthcombo.getValue() != '') {
//                        brasUtilStore.setBaseParam('fromdatetime', monthcombo.getValue()+'-'+Ext.getCmp('selyear').getValue());
//                        brasUtilStore.setBaseParam('todatetime', monthcombo.getValue()+'-'+Ext.getCmp('selyear').getValue());
//                        brasUtilStore.reload();
//                    }else
//                        Ext.MessageBox.show({
//                            title: 'No month selected',
//                            msg: 'Please select a month.',
//                            buttons: Ext.MessageBox.OK,
//                            icon: Ext.MessageBox.INFO
//                        });
//                }
//            }
//            
//        ]
    });
    
    function addCpuUsageGraph(averages,ranges,strTitle){
		
		
		var chart = new Ext.ux.HighChart({
			id: 'cpuusage_chart',
			series: [{
				name: 'Average CPU',
				data: averages,
				zIndex: 1,
				type: 'line',
				marker: {
					fillColor: 'white',
					lineWidth: 2,
					lineColor: Highcharts.getOptions().colors[0]
				}
			}, {
				name: 'Min-Max Range',
				data: ranges,
				lineWidth: 0,
				linkedTo: ':previous',
				color: Highcharts.getOptions().colors[0],
				fillOpacity: 0.3,
				zIndex: 0
			}],
			chartConfig: {
				chart: {
					//type: 'line',
					defaultSeriesType: 'arearange',
					zoomType: 'x',
					marginRight: 60
				},
				title: {
					text: strTitle,
					style: {
						font: 'normal 12px verdana'
					}
				},
		
				xAxis: {
					title: {
						enabled: true,
						text: 'Date Time'
					},
					type: 'datetime',
					dateTimeLabelFormats : {
						hour: '%I %p',
						minute: '%I:%M %p'
					}
				},			
				yAxis: {
					title: {
						text: '%'
					},
					labels: {
						style: {
							font: 'normal 10px Verdana, sans-serif'
						},
						formatter: function() {
							//return Highcharts.numberFormat(this.value, 0);
							return Highcharts.numberFormat(this.value, 2); //dua titik perpuluhan
						}
					},
					lineWidth: 1
				},			
				tooltip: {
					crosshairs: true,
					shared: true,
					valueSuffix: '%'
				},
				credits: {
					enabled: false
				},
				legend: {
					enabled:false
				}
			}
        });

         cpuUsagePanel = Ext.extend(Ext.Panel, {
            id:'CpuUsage_Panel',
            border:true,
            frame:true,
            layout: 'anchor',
            defaults: {anchor: '100%'},
            constructor : function (config) {
                    config = config || {};
                    var arguments = Ext.apply({
                                            layout: 'anchor',
                                            baseCls:'x-plain',
                                            anchor: '100% 50%',
                                            listeners: {
                                                resize : function(component, aw, ah) {
                                                    Ext.getCmp('cpuusage_chart').setWidth(aw);
                                                    Ext.getCmp('cpuusage_chart').setHeight(ah);
                                                }
                                            },
                                            items: [chart]//end items
                                    }, config);

                    cpuUsagePanel.superclass.constructor.call(this, arguments);
            }//end constructor()

        }); 
        Ext.reg('CpuUsage_ChartPanel', cpuUsagePanel);
        hypraceChartForm.add({xtype: 'CpuUsage_ChartPanel'});
        hypraceChartForm.doLayout();
        loadMask.hide();
    }; //end of addCpuUsageGraph
   
    var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});
    loadMask.show();
    
    var viewport = new Ext.Viewport
    ({
        layout : "border",
        items: hypraceChartForm
    });
    
});