Ext.apply(Ext.form.VTypes, {
	daterange : function(val, field) {
		var date = field.parseDate(val);
		if(!date){
			return;
		}
		if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
			var start = Ext.getCmp(field.startDateField);
			start.setMaxValue(date);
			start.validate();
			this.dateRangeMax = date;
		} 
		else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
			var end = Ext.getCmp(field.endDateField);
			end.setMinValue(date);
			end.validate();
			this.dateRangeMin = date;
		}
		return true;
	}
});

Ext.onReady(function() {
    var getParams = document.URL.split("?");
    var passParam = Ext.urlDecode(getParams[getParams.length - 1]);
    
    Ext.Ajax.request({   
            url: './../../php/getDiskHist.php',
	     params: { 
		    projectid:passParam['projectid'],
                  resourceid:passParam['resourceid'],
		    resourcename:passParam['resourcename']
            },
            callback: function (options, success, response) {
                    var json = Ext.util.JSON.decode(response.responseText);                    
                    if (json.avgmem.length == 0) 
			   Ext.MessageBox.show({
			   	title: 'Data not available',
				msg: 'No data found.',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.WARNING
			   });
		      addMemUsageGraph(json.avgmem,json.range,'Disk Usage for '+passParam['resourcename']+' (30 minutes interval)','Data from '+json.dtF+' to '+json.dtT);								                    
            }
    }); //end request
    
    var hypraceChartForm = new Ext.Panel({
        id:'chart-panel',
        border:true,
        region:'center',
        frame:false,
        defaults: {anchor: '100%'},		
        layout:'anchor'
    });
    
    function addMemUsageGraph(averages,ranges,strTitle,strSubTitle){
		
		var chart = new Ext.ux.HighChart({
			id: 'memusage_chart',
			series: [{
				name: 'Average Memory',
				data: averages,
				zIndex: 1,
				type: 'line',
				marker: {
					fillColor: 'white',
					lineWidth: 2,
					lineColor: Highcharts.getOptions().colors[0]
				}
			}, {
				name: 'Min-Max Range',
				data: ranges,
				lineWidth: 0,
				linkedTo: ':previous',
				color: Highcharts.getOptions().colors[0],
				fillOpacity: 0.3,
				zIndex: 0
			}],
			chartConfig: {
				chart: {
					//type: 'line',
					defaultSeriesType: 'arearange',
					zoomType: 'x',
					marginRight: 60
				},
				title: {
					text: strTitle,
					style: {
						font: 'normal 14px verdana'
					}
				},
				subtitle: {
					text: strSubTitle,
					style: {
						font: 'normal 11px verdana'
					}
				},
				xAxis: {
					title: {
						enabled: true,
						text: 'Date Time'
					},
					type: 'datetime',
					dateTimeLabelFormats : {
						hour: '%I %p',
						minute: '%I:%M %p'
					}
				},			
				yAxis: {
					title: {
						text: 'MB'
					},
					labels: {
						style: {
							font: 'normal 10px Verdana, sans-serif'
						},
						formatter: function() {
							//return Highcharts.numberFormat(this.value, 0);
							return Highcharts.numberFormat(this.value, 2); //dua titik perpuluhan
						}
					},
					lineWidth: 1
				},			
				tooltip: {
					crosshairs: true,
					shared: true,
					valueSuffix: 'MB'
				},
				credits: {
					enabled: false
				},
				legend: {
					enabled:false
				}
			}
        });

         memUsagePanel = Ext.extend(Ext.Panel, {
            id:'MemUsage_Panel',
            border:true,
            frame:true,
            layout: 'anchor',
            defaults: {anchor: '100%'},
            constructor : function (config) {
                    config = config || {};
                    var arguments = Ext.apply({
                                            layout: 'anchor',
                                            baseCls:'x-plain',
                                            anchor: '100% 50%',
                                            listeners: {
                                                resize : function(component, aw, ah) {
                                                    Ext.getCmp('memusage_chart').setWidth(aw);
                                                    Ext.getCmp('memusage_chart').setHeight(ah);
                                                }
                                            },
                                            items: [chart]//end items
                                    }, config);

                    memUsagePanel.superclass.constructor.call(this, arguments);
            }//end constructor()

        }); 
        Ext.reg('MemUsage_ChartPanel', memUsagePanel);
        hypraceChartForm.add({xtype: 'MemUsage_ChartPanel'});
        hypraceChartForm.doLayout();
        loadMask.hide();
    }; //end of addMemUsageGraph
   
    var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});
    loadMask.show();

    //####### BEGIN : search form ########
    var searchForm = new Ext.Panel({
        id:'searchForm',
        baseCls:'x-plain',
        border:false,
        region:'center',
        layout:'table',
        layoutConfig: {columns:5},
        //defaults: {frame:true, width:200, height: 200},
        items:[
		{
            xtype: 'fieldset',
            title: 'Date Range : ',
            autoHeight: true,
			width:370,
            labelWidth: 30,
            items: [{
                xtype : 'compositefield',
                anchor: '100%',
                fieldLabel: 'From',
                items : [
                    {
                        flex : 1,
                        xtype: 'datefield',
                        format : "d/M/Y",
                        name: 'dtfrom',
                        id: 'dtfrom',
                        allowBlank:true,
                        vtype: 'daterange',
                        endDateField: 'dtto'
                    },{
                        xtype: 'spinnerfield',
                        id: 'fspinnnerhour',
                        minValue: 1,
                        maxValue: 12,
                        allowDecimals: true,
                        width:40,
                        value:1,
                        accelerate: true
                    },{
                        xtype: 'displayfield',
                        value: ':'
                    },{
                        xtype: 'spinnerfield',
                        id: 'fspinnnerminutes',
                        minValue: 0,
                        maxValue: 59,
                        allowDecimals: true,
                        width:40,
                        value:0,
                        accelerate: true
                    },{
                        xtype: 'displayfield',
                        value: ':'
                    },{
                        xtype: 'spinnerfield',
                        id: 'fspinnnerseconds',
                        minValue: 0,
                        maxValue: 59,
                        allowDecimals: true,
                        width:40,
                        value:0,
                        accelerate: true
                    },{
                        width:          50,
                        xtype:          'combo',
                        mode:           'local',
                        value:          'AM',
                        triggerAction:  'all',
                        forceSelection: true,
                        editable:       false,
                        id:           'fampm',
                        hiddenName:     'fampm',
                        displayField:   'name',
                        valueField:     'value',
                        store:          new Ext.data.JsonStore({
                            fields : ['name', 'value'],
                            data   : [
                                {name : 'AM',   value: 'AM'},
                                {name : 'PM',  value: 'PM'}
                            ]
                        })
                    },
                ]
            },{
                xtype : 'compositefield',
                anchor: '100%',
                fieldLabel: 'To',
                items : [
                    {
                        flex : 1,
                        xtype: 'datefield',
                        format : "d/M/Y",
                        name: 'dtto',
                        id: 'dtto',
                        allowBlank:true,
                        vtype: 'daterange',
                        startDateField: 'dtfrom'
                    },{
                        xtype: 'spinnerfield',
                        id: 'tspinnnerhour',
                        minValue: 1,    
                        maxValue: 12,
                        allowDecimals: true,
                        width:40,
                        value:1,
                        accelerate: true
                    },{
                        xtype: 'displayfield',
                        value: ':'
                    },{
                        xtype: 'spinnerfield',
                        id: 'tspinnnerminutes',
                        minValue: 0,
                        maxValue: 59,
                        allowDecimals: true,
                        width:40,
                        value:0,
                        accelerate: true
                    },{
                        xtype: 'displayfield',
                        value: ':'
                    },{
                        xtype: 'spinnerfield',
                        id: 'tspinnnerseconds',
                        minValue: 0,
                        maxValue: 59,
                        allowDecimals: true,
                        width:40,
                        value:0,
                        accelerate: true
                    },{
                        width:          50,
                        xtype:          'combo',
                        mode:           'local',
                        value:          'AM',
                        triggerAction:  'all',
                        forceSelection: true,
                        editable:       false,
                        id:           'tampm',
                        hiddenName:     'tampm',
                        displayField:   'name',
                        valueField:     'value',
                        store:          new Ext.data.JsonStore({
                            fields : ['name', 'value'],
                            data   : [
                                {name : 'AM',   value: 'AM'},
                                {name : 'PM',  value: 'PM'}
                            ]
                        })
                    }
                ]
            }]
        },{xtype: 'box',baseCls:'x-plain',width:5},
		{
            xtype: 'fieldset',
            title: 'Interval : ',
            autoHeight: true,
			width:175,
            labelWidth: 30,
            items: [{
                xtype : 'compositefield',
                anchor: '100%',
                fieldLabel: 'Every',
                items : [
                    {
                        xtype: 'spinnerfield',
                        id: 'fspinnnerinterval',
                        minValue: 1,
                        maxValue: 1000,
                        allowDecimals: true,
                        width:40,
                        value:30,
                        accelerate: true
                    },{
                        width:          70,
                        xtype:          'combo',
                        mode:           'local',
                        value:          'Minutes',
                        triggerAction:  'all',
                        forceSelection: true,
                        editable:       false,
                        id:           'finterval',
                        hiddenName:     'finterval',
                        displayField:   'name',
                        valueField:     'value',
                        store:          new Ext.data.JsonStore({
                            fields : ['name', 'value'],
                            data   : [
                                {name : 'Hour',   value: 'hour'},
                                {name : 'Minutes',  value: 'minute'}
                            ]
                        })
                    }
                ]
            },{xtype: 'box',baseCls:'x-plain',height:27}]
		},{xtype: 'box',baseCls:'x-plain',width:10},
		new Ext.Button({
			id: 'btnViewChart',
			text: '&nbsp;&nbsp;View Chart&nbsp;&nbsp;',
			iconCls: 'linechart',
			width:81,
			height:81,
			scale: 'large',
			iconAlign: 'top',
			handler: function(){
				queryData();
			}
		})]
    });
    //####### END : search form ##########

    function queryData() {
		if (Ext.get('dtfrom').getValue()!='' && Ext.get('dtto').getValue()!=''){
                    var tFrom = String.leftPad(Ext.get('fspinnnerhour').getValue(), 2, '0') + ':' +
                                String.leftPad(Ext.get('fspinnnerminutes').getValue(), 2, '0') + ':' +
                                String.leftPad(Ext.get('fspinnnerseconds').getValue(), 2, '0') + ' ' + 
                                Ext.get('fampm').getValue();
                    var tTo = String.leftPad(Ext.get('tspinnnerhour').getValue(), 2, '0') + ':' +
                              String.leftPad(Ext.get('tspinnnerminutes').getValue(), 2, '0') + ':' +
                              String.leftPad(Ext.get('tspinnnerseconds').getValue(), 2, '0') + ' ' +  
                              Ext.get('tampm').getValue();
                    var tempDtfrom = Ext.get('dtfrom').getValue() + ' ' + tFrom;
                    var tempDtto = Ext.get('dtto').getValue() + ' ' + tTo;
                    var dtF = new Date();
                    dtF = Date.parseDate(tempDtfrom, "d/M/Y h:i:s A");
                    var dtT = new Date();
                    dtT = Date.parseDate(tempDtto, "d/M/Y h:i:s A");
		      var fInterval = parseInt(Ext.get('fspinnnerinterval').getValue());
		      if (Ext.get('finterval').getValue() == 'Hour') fInterval = fInterval * 60;
		      fInterval = fInterval * 60;
		      loadMask.show();
		      Ext.Ajax.request({   
            			url: './../../php/getMemHist.php',
	     			params: { 
				    projectid:passParam['projectid'],
              		    resourceid:passParam['resourceid'],
				    resourcename:passParam['resourcename'],
				    dtFrm:dtF.format('Y-m-d H:i:s'),
              		    dtTo:dtT.format('Y-m-d H:i:s'),
				    freqInterval:fInterval
		              },
            			callback: function (options, success, response) {
				      loadMask.hide();
		                    var json = Ext.util.JSON.decode(response.responseText);  
				      if(hypraceChartForm.getComponent('MemUsage_Panel'))
	                                hypraceChartForm.remove('MemUsage_Panel');	                  
                    		      if (json.avgmem.length == 0) 
			   			Ext.MessageBox.show({
			   				title: 'Data not available',
							msg: 'No data found.',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.WARNING
			   			});
		      			addMemUsageGraph(json.avgmem,json.range,'Memory Usage for '+passParam['resourcename']+' ('+
							   Ext.get('fspinnnerinterval').getValue()+' '+Ext.get('finterval').getValue()+
							   ' interval)','Data from '+json.dtF+' to '+json.dtT);								                    
					
            			}
    			}); //end request
                }else
			Ext.MessageBox.show({
				title: 'Empty Search Options',
				msg: 'Search options is required',
				buttons: Ext.MessageBox.OK,
				icon: Ext.MessageBox.INFO
			 });
    };
    
    var viewport = new Ext.Viewport
    ({
        layout : "border",
        items:[ 
		{
                region: 'north',
                border:false,
				//margins: '0 0 0 0',
				height: 110,
				bodyStyle:'background: #eaeaea;padding:10px;',
                margins: '0 0 0 0',
                layout: 'fit', // specify layout manager for items
                items: searchForm  
            	},hypraceChartForm
	 ]
    });
    
});